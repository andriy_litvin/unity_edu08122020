﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC1 : MonoBehaviour
{
    public float radiusAttack = 10f; // радіус атаки
    public Rigidbody bullet; // куля
    public Transform shotPoint; // точка для створення кулі
    bool isShot = true; // ознака того, що можна стріляти
    Transform player; // гравець

    private void Start()
    {
        player = FindObjectOfType<PlayerController3d>().transform;
    }

    void Update()
    {
        // якщо можна стріляти
        if (isShot)
        {
            // якщо гравець в радіусі атаки, виконуємо постріл по ньому самонавідною кулею
            if (Vector3.Distance(transform.position, player.position) < radiusAttack)
            {
                // напрямок пострілу на гравця
                shotPoint.LookAt(player);
                // встановлення цілі - гравець
                ShotTower(player);
            }
            // інакше постріл в довільному напрямку звичайною кулею
            else
            {
                // довільний напрямок пострілу
                shotPoint.localEulerAngles = new Vector3(0, Random.Range(0f, 360f), 0);
                // ціль не встановлена
                ShotTower(null);
            }
        }
    }

    // вистріл кулею
    void ShotTower(Transform target)
    {
        Rigidbody temp = Instantiate(bullet, shotPoint.position, Quaternion.identity);
        temp.AddForce(shotPoint.TransformDirection(0, 0, 1000));
        isShot = false;
        temp.GetComponent<BulletTower>().target = target;
        StartCoroutine(TimerShot());
    }

    // затримка між вистрілами 
    IEnumerator TimerShot()
    {
        yield return new WaitForSeconds(2);
        isShot = true;
    }
}
