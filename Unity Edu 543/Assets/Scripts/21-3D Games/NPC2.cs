﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class NPC2 : MonoBehaviour
{
    public Transform[] area; // масив для двох точок на сцені, що описують зону патрулювання 
    NavMeshAgent agent;
    Animator anim;
    Vector3 pointPatrul; // точка до якої рухається НПЦ під час патрулювання
    Transform playerTransform; // посилання на гравця
    PlayerController3d playerControl;
    bool isAttackWalk = false; // стан атака-переслідування
    bool isAttack = false; // стан атаки з нанесенням пошкоджень гравцю
    int hp = 30; // життя ПНЦ

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        NextPointPatrul();
        playerControl = FindObjectOfType<PlayerController3d>();
        playerTransform = playerControl.transform;
    }

    void Update()
    {
        // передаємо параметр швидкості руху НПЦ для перемикання анімацій
        anim.SetFloat("speed", agent.velocity.magnitude);
        // визначаємо відстань між гравцем та НПЦ
        float distanc = Vector3.Distance(transform.position, playerTransform.position);
        // якщо відстань менша за 15
        if (distanc < 15)
        {
            // якщо не переслідуємо гравця, то шукаємо його	
            if (!isAttackWalk)
            {
                Debug.DrawRay(transform.position, (playerTransform.position - transform.position)*15, Color.red);
                RaycastHit hit;
                // якщо побачили гравця, що не сховався від нас за стіною - починаємо переслідувати його
                // вмикаємо стан переслідування
                if (Physics.Raycast(transform.position, playerTransform.position - transform.position, out hit))
                {
                    if (hit.transform.tag == "Player")
                    {
                        isAttackWalk = true;
                    }
                }
            }
            // якщо переслідуємо гравця
            else
            {
                // якщо відстань до гравця менша за 2, переходимо в стан атаки
                if (distanc < 2 && !isAttack)
                {
                    agent.SetDestination(transform.position);
                    //agent.isStopped = true;
                    isAttack = true;
                    anim.SetTrigger("attack");
                }
                else
                {    
                // вказуємо рух до точки, де знаходиться гравець
                agent.SetDestination(playerTransform.position);
                }
            }
        }
        // якщо відстань до гравця більша рівна 15 і активний стан переслідування, 
        // то виходимо зі стану переслідування і рухаємося до точки де, останній раз був побачений гравець
        else if (isAttackWalk)
        {
            isAttackWalk = false;
            agent.SetDestination(playerTransform.position);
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "bulet")
        {
            DamageNPC();
        }
    }
    // наступна випадкова точка для патрулювання
    void NextPointPatrul()
    {
        pointPatrul = Vector3.zero;
        for (int i = 0; i < 3; i++)
        {
            pointPatrul[i] = Random.Range(area[0].position[i], area[1].position[i]);
        }
        agent.SetDestination(pointPatrul);
        StartCoroutine(Stoped());
    }

    // корутина, що відслідковує чи зупинився НПЦ, якщо так то це означає, що він прийшов до кінцевої точки (або 
    // майже прийшов до кінцевої точки або по дорозі зустрів перешкоду яка блокує шлях)
    IEnumerator Stoped()
    {
        yield return new WaitForSeconds(1);

        while (true)
        {
            yield return null;
            // якщо швидкість менша за 0.1
            if (agent.velocity.magnitude < 0.1)
            {
                // поточну почицію встановлюємо кінцевою
                agent.SetDestination(transform.position);
                // чекаємо 3 секунди щоб НПЦ "подумав" куди йти далі
                yield return new WaitForSeconds(3);
                // визначаємо нову точку для патрулювання
                NextPointPatrul();
                // вихід з циклу, щоб завершить роботу поточної корутини
                break;
            }
        }
    }

    // нанесення пошкоджень гравцю, викликається через анімацію
    public void Damage()
    {
        playerControl.DamageToPlayer(20);
    }

    // вихід зі стану атаки, викликається через анімацію
    public void EndAttack()
    {
        isAttack = false;

    }

    // нанесення шкоди НПЦ
    public void DamageNPC()
    {
        hp -= 10;
        if (hp <= 0)
        {                      
            StopAllCoroutines();
            Destroy(gameObject);
        }
    }


}
