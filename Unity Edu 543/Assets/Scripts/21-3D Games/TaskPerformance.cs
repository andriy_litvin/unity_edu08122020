﻿using UnityEngine;

public class TaskPerformance : MonoBehaviour
{
    [SerializeField] private int idTask;  // ідентифікаток завдання до якого налещить предмет

    // при знищенні об'єкта викликаємо метод додавання виконаної дії до відповідного завдання
    private void OnDestroy()
    {
        try
        {
            SysTask.sys.AddItemTask(idTask);
        }
        catch (MissingReferenceException)
        {
            print("my MissingReferenceException");
        }
    }
}
