﻿using System.Collections.Generic;
using UnityEngine;

public class SysTask : MonoBehaviour
{
    public static SysTask sys;
    [SerializeField] private GameTask prefTask;  // префаб для завдання
    [SerializeField] private Transform taskContainer; // контейнер для упорадкування завдань
    [SerializeField] private GameObject panelWinner;  // панель, що з'являєтьсь коли виконані всі завдання (виграш)
    [SerializeField] private List<MyTask> listTask;   // список завдань - початкові дані для формування елементів інтерфейсу 
    private List<GameTask> panelsTask;    // список з посиланнями на окремі панелі для завдань

    private void Awake()
    {
        sys = this;
    }

    void Start()
    {
        panelsTask = new List<GameTask>();
        for (int i = 0; i < listTask.Count; i++)
        {
            panelsTask.Add(Instantiate(prefTask, Vector3.zero, Quaternion.identity, taskContainer));
            panelsTask[i].Init(listTask[i].id, listTask[i].task, listTask[i].countTarget);
        }
    }

    // метод додавання виконаної дії до відповідного завдання
    public void AddItemTask(int id)
    {
        for (int i = 0; i < panelsTask.Count; ++i)
        {
            if (panelsTask[i].id == id)
            {
                if (panelsTask[i].AddCount())
                {
                    panelsTask.RemoveAt(i);
                }
            }
        }
        IsWinner();
    }

    // виграш у випадку виконання всіх завдань
    // відкриття відповідного повідомлення
    void IsWinner()
    {
        if (panelsTask.Count == 0)
        {
            panelWinner.SetActive(true);
            Time.timeScale = 0;
        }
    }
}

// клас для опису завдань
[System.Serializable]
public class MyTask
{
    public int id;
    public string task;
    public int countTarget;
}