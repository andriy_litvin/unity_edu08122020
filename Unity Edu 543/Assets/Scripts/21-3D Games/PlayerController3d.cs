﻿using UnityEngine;

public class PlayerController3d : MonoBehaviour
{
    public float speed = 3;
    public float mouseSensety = 2;
    public Rigidbody bullet;
    public Transform gun;

    [SerializeField] private int hp = 100;
    private Transform _transform;
    private Rigidbody _rigidbody;
    private Transform camera;
    private Vector3 rotCam;
    private float currentSpeed;

    private bool isGround = false;
    [SerializeField] private AudioSource audioSourceStep;
    [SerializeField] private AudioSource audioSourceShot;
    [SerializeField] private AudioClip step;
    [SerializeField] private AudioClip shot;


    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _transform = transform;
        camera = Camera.main.transform;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        currentSpeed = speed;
               
        audioSourceStep.clip = step;
    }

    void Update()
    {
        _rigidbody.velocity = _transform.TransformDirection(Input.GetAxis("Horizontal") * currentSpeed, _rigidbody.velocity.y, Input.GetAxis("Vertical") * currentSpeed);
        _transform.Rotate(0, Input.GetAxis("Mouse X") * mouseSensety, 0, Space.World);
        rotCam = camera.localEulerAngles;
        camera.Rotate(-Input.GetAxis("Mouse Y") * mouseSensety, 0, 0, Space.Self);
        if((camera.localEulerAngles.x>310 && camera.localEulerAngles.x<360)||(camera.localEulerAngles.x>0&& camera.localEulerAngles.x<30))
        { }
        else
        {
            camera.localEulerAngles = rotCam;
        }

        if(Input.GetKeyDown(KeyCode.C))
        {
            Cursor.visible = !Cursor.visible;
            Cursor.lockState = Cursor.visible ? CursorLockMode.None : CursorLockMode.Locked;
        }

        isGround = Physics.Raycast(_transform.position, Vector3.down, 1.2f);
        if(Input.GetKeyDown(KeyCode.Space)&&isGround)
        {
            _rigidbody.AddForce(0, 300, 0);
        }

        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            currentSpeed *= 2;
        }
        else if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            currentSpeed = speed;
        }

        if(Input.GetMouseButtonDown(0))
        {
            Rigidbody temp = Instantiate(bullet, gun.position, Quaternion.identity);
            temp.AddForce(gun.TransformDirection(0,0,1000));
            audioSourceShot.PlayOneShot(shot);
        }
        else if(Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                obj.transform.position = hit.point;
                obj.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                obj.GetComponent<MeshRenderer>().material.color = Color.black;
            }
        }

        if(_rigidbody.velocity.magnitude>0.1f && isGround && !audioSourceStep.isPlaying)
        {
            audioSourceStep.Play();
            print("play");
        }
        else if(audioSourceStep.isPlaying && _rigidbody.velocity.magnitude < 0.1f)
        {
            audioSourceStep.Stop();
            print("stop");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "buletNPC")
        {
            DamageToPlayer(10);  
        }
    }

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Bonus")
        {
            Destroy(coll.gameObject);
        }
    }

    public void DamageToPlayer(int damage)
    {
        hp -= damage;
        print($"HP: {hp}");
        if (hp <= 0)
        {
            Time.timeScale = 0;
            print("GAME OVER");
        }
    }

}
