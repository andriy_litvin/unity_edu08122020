﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Audio;

public class SoundSettings : MonoBehaviour
{
    public Canvas canvas;    
    public AudioMixer audioMixer;
    public AudioMixerSnapshot snapshotNormal;
    public AudioMixerSnapshot snapshotEcho;

    void Start()
    {
        canvas.enabled=false;  
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            canvas.enabled = !canvas.enabled;
            if(canvas.enabled)
            {
                snapshotEcho.TransitionTo(1);
            }
            else
            {
                snapshotNormal.TransitionTo(1);
            }
        }
    }

    public void SetVolume(float volume)
    {
        //   0  -  1
        // -80  -  0
        audioMixer.SetFloat("Volume", Mathf.Lerp(-80, 0, volume));
    }
        
}
