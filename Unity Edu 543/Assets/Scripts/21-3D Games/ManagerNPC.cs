﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerNPC : MonoBehaviour
{
    [SerializeField] private int maxCountNPC;
    [SerializeField] private NPC2 monstr;
    [SerializeField] private Transform[] area;    

    void Start()
    {
        StartCoroutine(CountNPCSnene());   
    }

    IEnumerator CountNPCSnene()
    {
        while (true)
        {
            yield return new WaitForSeconds(5);
            if (transform.childCount < maxCountNPC)
            {
                NPC2 npc = Instantiate(monstr, transform.position, Quaternion.identity, transform);
                npc.area = area;
            }
        }
    }

}
