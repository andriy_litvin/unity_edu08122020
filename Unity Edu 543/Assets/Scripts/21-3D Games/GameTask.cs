﻿using UnityEngine;
using UnityEngine.UI;

public class GameTask : MonoBehaviour
{
    public int id;   // ідентифікатор завдання
    [SerializeField] private Text textTask;  // текс завдання
    [SerializeField] private Text textCountTarget;  // текст з прогресом виконання завдання
    [SerializeField] private Image imageFlag; // прапорець, що відображається коли завдання виконано
    private int countTarget;   // кількість дій, що потрібно зробити щоб виконати завдання
    private int countCarrent;  // поточна кількість дій, що виконана

    // ініціалізація параметрів завдання
    public void Init(int id, string newTast, int newTargetTask)
    {
        this.id = id;
        textTask.text = newTast;
        countTarget = newTargetTask;
        textCountTarget.text = $"{countCarrent}/{countTarget}"; //   0/5  ->  1/5   5/5 (flag)
        imageFlag.enabled = false;
    }

    // збільшення лічильника виконаних дій, оновлення текстового поля з лічильником
    // повертає true якщо завдання виконано
    public bool AddCount()
    {
        countCarrent++;
        if (countCarrent == countTarget)
        {
            textCountTarget.enabled = false;
            imageFlag.enabled = true;
            return true;
        }
        else
        {
            textCountTarget.text = $"{countCarrent}/{countTarget}";
            return false;
        }
    }
}