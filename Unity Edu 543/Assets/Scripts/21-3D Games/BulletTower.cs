﻿using UnityEngine;

public class BulletTower : MonoBehaviour
{
    public float speed = 5f;  // швидкість кулі
    public Transform target; // посилання на ціль кулі
    private Rigidbody rb;
    private bool isBig=false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // якщо встановлена ціль, то куля рухається на неї змінюючи траєкторію руху
        if (target)
        {
            transform.LookAt(target);
            rb.velocity = transform.TransformDirection(0, 0, speed);
        }
    }

    // колізія кулі
    private void OnCollisionEnter(Collision collision)
    {
        // з гравцем
        if (collision.transform.tag == "Player")
        {
            Destroy(gameObject);
        }
        // з кулею гравця
        else if (collision.transform.tag == "bulet")
        {
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
        // з іншим об'єктом. Збльшується у розмірі та через 3 секунди видаляється
        else if(!isBig)
        {
            transform.localScale *= 3; //new Vector3(3, 3, 3);
            Destroy(gameObject, 3);
            isBig = true;
        }
    }
}
