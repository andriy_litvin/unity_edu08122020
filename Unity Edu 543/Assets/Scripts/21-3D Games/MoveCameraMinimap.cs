﻿using UnityEngine;

public class MoveCameraMinimap : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private int distance=100;

    void Update()
    {
        transform.position = player.transform.position + Vector3.up * distance;  
    }
}
