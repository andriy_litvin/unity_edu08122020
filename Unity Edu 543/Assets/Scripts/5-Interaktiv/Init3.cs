﻿using UnityEngine;

public class Init3 : MonoBehaviour
{
    public int countStars = 12;
    public Star3 starPref;
    public Transform marker1;
    public Transform marker2;
    private int countGreen = 0;
    private int countFreez = 0;
    private bool isGameOver = false;
    private float timer = 5;

    void Start()
    {
        for (int i = 0; i < countStars; i++)
        {
            Star3 temp = Instantiate(starPref, new Vector3(Random.Range(marker1.position.x, marker2.position.x),
                Random.Range(marker1.position.y, marker2.position.y), 0), Quaternion.identity, transform);
            temp.SetInit(this);
        }
    }

    void Update()
    {
        if (isGameOver)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(0);
                isGameOver = false;
            }
        }
    }

    private void GameOver()
    {
        if (countGreen + countFreez == countStars)
        {
            print($"Гра завершена! Ви зібрали зелиних {countGreen}. А заморозили {countFreez}");
            isGameOver = true;
        }
    }

    public void AddCountGreenOrFreez(bool isGreen)
    {
        if (isGreen)
        {
            countGreen++;
        }
        else
        {
            countFreez++;
        }
        GameOver();
    }


}
