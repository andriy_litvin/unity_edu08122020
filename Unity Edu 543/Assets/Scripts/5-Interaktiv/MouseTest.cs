﻿using UnityEngine;

public class MouseTest : MonoBehaviour
{
    // при наведенні миші на об'єкт - 1р. 
    private void OnMouseEnter()
    {
        print("OnMouseEnter");
    }

    // при відвиденні миші з об'єкту - 1р.
    private void OnMouseExit()
    {
        print("OnMouseExit");
    }

    // при перебуванні миші на об'єкті - щокадру
    private void OnMouseOver()
    {
        print("OnMouseOver");
    }

    // при натасканні кнопки на об'єкті - 1р.
    private void OnMouseDown()
    {
        print("OnMouseDown");
    }

    // при відпусканні кнопки на об'єкті - 1р.
    private void OnMouseUp()
    {
        print("OnMouseUp");
    }

    // при перетягуванні на об'єкті - щокадру
    private void OnMouseDrag()
    {
        print("OnMouseDrag");
    }

    // при натисканні та відпусканні на тому самому об'єкті
    private void OnMouseUpAsButton()
    {
        print("OnMouseUpAsButton");
    }

}
