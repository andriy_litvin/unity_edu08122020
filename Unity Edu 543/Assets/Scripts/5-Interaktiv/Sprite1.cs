﻿using UnityEngine;

public class Sprite1 : MonoBehaviour
{
    public SpriteRenderer sr;
    private int idI;
    private int idJ;
    private Init1 init;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();   
    }

    private void OnMouseEnter()
    {
        init.SelectedSprite(idJ, idI);  
    }

    private void OnMouseExit()
    {
        init.UnSelectedSprite();
    }

    public void Set(int idI, int idJ, Init1 init)
    {
        this.idI = idI;
        this.idJ = idJ;
        this.init = init;
    }      
}
