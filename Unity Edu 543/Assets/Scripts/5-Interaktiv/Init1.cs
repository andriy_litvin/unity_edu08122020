﻿using UnityEngine;

public class Init1 : MonoBehaviour
{
    public int countCol = 5;
    public int countRow = 4;
    public float deltaPos = 0.1f;
    public Sprite1 spritePref;
    private Sprite1[,] sprites;
    private int selectIdI = 0;
    private int selectIdJ = 0;

    void Start()
    {
        sprites = new Sprite1[countRow, countCol];
        for (int i = 0; i < countRow; i++)
        {
            for (int j = 0; j <countCol; j++)
            {
            sprites[i,j] = Instantiate(spritePref, new Vector3(j * deltaPos, i*deltaPos, 0), Quaternion.identity, transform);
            sprites[i,j].Set(j,i,this);
            }
        }
    }

    public void SelectedSprite(int idI, int idJ)
    {
        selectIdI = idI;
        selectIdJ = idJ;
        sprites[idI,idJ].sr.color = Color.red;
        for (int i = selectIdI-1; i <= selectIdI+1; i++)
        {
            for (int j = selectIdJ-1; j <= selectIdJ+1; j++)
            {
                if (i == idI && j == idJ)
                    continue;
                if (i >= 0 && i < countRow && j >= 0 && j < countCol)
                {
                    sprites[i,j].sr.color = Color.green;  
                }
            }
        }               
    }

    public void UnSelectedSprite()
    {
        for (int i = selectIdI - 1; i <= selectIdI + 1; i++)
        {
            for (int j = selectIdJ - 1; j <= selectIdJ + 1; j++)
            {
                if (i >= 0 && i < countRow && j >= 0 && j < countCol)
                {
                    sprites[i, j].sr.color = Color.white;
                }
            }
        }     
    }      
}
