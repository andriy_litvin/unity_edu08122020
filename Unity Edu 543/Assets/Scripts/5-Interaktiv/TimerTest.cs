﻿using UnityEngine;

public class TimerTest : MonoBehaviour
{
    private float timer = 0;
    private bool isActiveTimer = true;

    private void Update()
    {
        if (isActiveTimer)
        {
            timer += Time.deltaTime;
            if (timer > 5)
            {
                isActiveTimer = false;
                print("Пройшло 5 секунд");
            }
        }
    }
                        
    /*
    void Update()
    {
        timer += Time.deltaTime; // 0.03   
        if (timer > 5)
        {
            timer = 0;
            print("Пройшло 5 секунд");
        }
    }  */

}
