﻿using UnityEngine;

public class Star3 : MonoBehaviour
{
    private SpriteRenderer sr;
    private float timer = 0;
    private bool isRed = false;
    private bool isFreez = false;
    private Init3 init;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        timer = Random.Range(1f, 3f);
        sr.color = Color.green;
    }

    void Update()
    {
        if (!isFreez)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                timer = Random.Range(1f, 3f);
                if (!isRed) // якщо зелений
                {
                    isRed = true;
                    sr.color = Color.red;
                }
                else  // якщо червоний
                {
                    isRed = false;
                    sr.color = Color.green;
                }
            }
        }
    }

    private void OnMouseUpAsButton()
    {
        if (!isRed)  // зелений true
        {
            Destroy(gameObject);
        }
        else // червоний  false
        {
            Freez();
        }
        init.AddCountGreenOrFreez(!isRed);
    }

    private void Freez()
    {
        isFreez = true;
        sr.color = new Color(1, 0, 0, 0.4f);
        GetComponent<CircleCollider2D>().enabled = false;
    }

    public void SetInit(Init3 init)
    {
        this.init = init;
    }

}
