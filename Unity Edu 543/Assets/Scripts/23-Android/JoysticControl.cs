﻿using UnityEngine;

public class JoysticControl : MonoBehaviour
{
    public Transform joy;
    public JoyPlayer player;
    Vector3 targetVector, touchPos;

    void Start()
    {
        joy.position = transform.position;
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            touchPos = Input.GetTouch(0).position;
            joy.position = Vector3.MoveTowards(transform.position, touchPos, 100);
            targetVector = joy.position - transform.position;
            targetVector = Vector3.ClampMagnitude(targetVector, 100);
            player.move = targetVector;
        }
        else
        {
            joy.position = transform.position;
            player.move = Vector3.zero;
        }
    }
}