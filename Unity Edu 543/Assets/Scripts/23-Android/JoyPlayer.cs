﻿using UnityEngine;

public class JoyPlayer : MonoBehaviour
{
    public float speed = 1;
    public Vector3 move;
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        //rb.velocity = move * speed * Time.deltaTime;
        rb.AddForce(move.x*speed, 0, move.y*speed);
    }
}