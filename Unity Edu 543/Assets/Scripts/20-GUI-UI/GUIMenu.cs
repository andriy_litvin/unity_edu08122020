﻿using UnityEngine;

public class GUIMenu : MonoBehaviour
{
    private int idMenu = 0;
    private bool tog1 = false, tog2 = false, tog3 = false;

    private void OnGUI()
    {
        if (idMenu == 0)
        {
            GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 70, 200, 140));
            GUI.Box(new Rect(0, 0, 200, 140), "Main menu");
            if (GUI.Button(new Rect(10, 20, 180, 30), "Game"))
            {
                idMenu = 1;
            }
            else if (GUI.Button(new Rect(10, 60, 180, 30), "Settings"))
            {
                idMenu = 2;
            }
            else if (GUI.Button(new Rect(10, 100, 180, 30), "Exit"))
            {
                idMenu = 3;
            }
            GUI.EndGroup();
        }
        else if (idMenu == 2)
        {
            GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 80, 200, 160));
            GUI.Box(new Rect(0, 0, 200, 160), "Settings");
            tog1 = GUI.Toggle(new Rect(10, 20, 180, 20), tog1, "Toggle1");
            tog2 = GUI.Toggle(new Rect(10, 50, 180, 20), tog2, "Toggle2");
            tog3 = GUI.Toggle(new Rect(10, 80, 180, 20), tog3, "Toggle3");

            if (GUI.Button(new Rect(10, 120, 180, 30), "Cancel"))
            {
                idMenu = 0;
            }
            GUI.EndGroup();
        }


    }
}
