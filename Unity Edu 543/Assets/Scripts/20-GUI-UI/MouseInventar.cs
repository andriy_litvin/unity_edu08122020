﻿using UnityEngine;
using UnityEngine.UI;

public class MouseInventar : MonoBehaviour
{    
    public Image image;
    public Text textCount;
    public int count;
    public string nameItem;
    public int idCellMuve;
    private Transform _transform;

    private void Awake()
    {        
        image = GetComponent<Image>();
        textCount = GetComponentInChildren<Text>();
        image.color = new Color(1, 1, 1, 0);
        textCount.text = "";
        _transform = transform;
    }

    void Update()
    {
        _transform.position = Input.mousePosition;   
    }

    public void SetCount(int count)
    {
        this.count = count;
        textCount.text = count == 0 ? "" : count.ToString();
    }

}
