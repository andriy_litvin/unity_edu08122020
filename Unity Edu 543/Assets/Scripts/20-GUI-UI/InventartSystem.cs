﻿using UnityEngine;

public class InventartSystem : MonoBehaviour
{
    public static InventartSystem sys;
    public ItemInventar[] itemsInv;
    [SerializeField] private GameObject panelDelete;
    private Canvas canvas;
    private int idDelete;
    private MouseInventar mouse;

    private void Awake()
    {
        sys = this;
        itemsInv = GetComponentsInChildren<ItemInventar>();
        canvas = GetComponentInParent<Canvas>();
        mouse = FindObjectOfType<MouseInventar>();
        for (int i = 0; i < itemsInv.Length; i++)
        {
            itemsInv[i].idCell = i;
        }

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            canvas.enabled = !canvas.enabled;
        }
    }

    public void AddToInventar(Sprite sprite, string nameItem)
    {
        int countElemInv = itemsInv.Length;
        for (int i = 0; i < countElemInv; i++)
        {
            if (itemsInv[i].nameItem == nameItem && itemsInv[i].countItem > 0)
            {
                itemsInv[i].SetCount(itemsInv[i].countItem + 1);
                return;
            }
        }

        for (int i = 0; i < countElemInv; i++)
        {
            if(!itemsInv[i].image.sprite)
            {
                itemsInv[i].image.sprite = sprite;
                itemsInv[i].image.color = Color.white;
                itemsInv[i].SetCount(1);
                itemsInv[i].nameItem = nameItem;
                return;
            }
        }
    }

    public void OpenMenuDelete()
    {
        print("drop delete");
        panelDelete.SetActive(true);
        idDelete = mouse.idCellMuve;
    }

    public void DeleteYes()
    {
        panelDelete.SetActive(false);
        itemsInv[idDelete].image.sprite = null;
        itemsInv[idDelete].image.color = new Color(1, 1, 1, 0);
        itemsInv[idDelete].SetCount(0);
        itemsInv[idDelete].nameItem = "";
    }

    public void DeleteNo()
    {
        panelDelete.SetActive(false);
    }

}