﻿using UnityEngine;

[ExecuteInEditMode]
public class GUIDemo : MonoBehaviour
{
    public Rect rectGroup;
    public string textField1 = "";
    public float valSlider, leftSlider, rightSlider = 5;
    public float valScrollbar, sizeScrollbar = 1, leftScrollbar, rightScrollbar = 10;
    public Texture texture;
    public Rect window1, window2;
    public GUISkin skin;
    private string passwordField = "";
    private string textArea1 = "";
    private bool panel1 = false;
    private bool tog1 = false, tog2 = false, tog3 = false;
    private int idTool = 0;
    private string[] itemsMenu = { "Menu 1", "Menu 2", "Menu 3" };

    private void OnGUI()
    {
        GUI.skin = skin;
        if (panel1)
        {
            GUI.BeginGroup(rectGroup);
            GUI.Label(new Rect(20, 20, 200, 30), $"Screen ({Screen.width}, {Screen.height})");
            textField1 = GUI.TextField(new Rect(20, 60, 200, 30), textField1);
            passwordField = GUI.PasswordField(new Rect(20, 100, 200, 30), passwordField, '*');
            textArea1 = GUI.TextArea(new Rect(20, 140, 200, 150), textArea1);
            GUI.EndGroup();
        }
        else
        {
            GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 200));
            GUI.Box(new Rect(0, 0, 200, 200), "Menu");
            valSlider = GUI.HorizontalSlider(new Rect(10, 30, 180, 20), valSlider, leftSlider, rightSlider);
            valScrollbar = GUI.HorizontalScrollbar(new Rect(10, 60, 180, 20), valScrollbar, sizeScrollbar, leftScrollbar, rightScrollbar);
            GUI.DrawTexture(new Rect(10, 90, valSlider / rightSlider * 180, 20), texture);
            GUI.EndGroup();
        }

        if (GUI.Button(new Rect(Screen.width - 110, 10, 100, 30), "Click me"))
        {
            panel1 = !panel1;
        }

        tog1 = GUI.Toggle(new Rect(20, Screen.height - 100, 180, 20), tog1, "Toggle1");
        tog2 = GUI.Toggle(new Rect(20, Screen.height - 70, 180, 20), tog2, "Toggle2");
        tog3 = GUI.Toggle(new Rect(20, Screen.height - 40, 180, 20), tog3, "Toggle3");


        idTool = GUI.Toolbar(new Rect(Screen.width / 2 - 125, 10, 250, 30), idTool, itemsMenu);
        if(idTool==0)
        {
            GUI.Label(new Rect(Screen.width-200, Screen.height-20, 200, 20), "penal 1");
            window1 = GUI.Window(0, window1, PaintWindow, "Window 1");
        }
        else if(idTool==1)
        {
            GUI.Label(new Rect(Screen.width - 200, Screen.height - 20, 200, 20), "penal 2");
            window2 = GUI.Window(1, window2, PaintWindow, "Window 2");
        }
        else
        {
            GUI.Label(new Rect(Screen.width - 200, Screen.height - 20, 200, 20), "penal 3");
        }
    }

    void PaintWindow(int id)
    {
        if (id == 0)
        {
            textField1 = GUI.TextField(new Rect(10, 30, 180, 20), textField1);
        }
        else if (id == 1)
        {
            passwordField = GUI.PasswordField(new Rect(10, 50, 180, 20), passwordField, '*');
        }
        GUI.DragWindow();
    }

}
