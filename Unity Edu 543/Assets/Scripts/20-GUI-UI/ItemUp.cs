﻿using UnityEngine;

public class ItemUp : MonoBehaviour
{
    [SerializeField] private string nameItem;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            InventartSystem.sys.AddToInventar(GetComponent<SpriteRenderer>().sprite, nameItem);
            if (gameObject) Destroy(gameObject);
        }
    }
}
