﻿using UnityEngine;
using UnityEngine.UI;

public class UIDemo : MonoBehaviour
{
    [SerializeField] private Toggle toggleLevel1;
    [SerializeField] private Toggle toggleLevel2;

    public void ExitGame()
    {
        Application.Quit();
    }

    public void LoadNewGame()
    {
        if(toggleLevel1.isOn)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
        else if(toggleLevel2.isOn)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        }
    }


}
