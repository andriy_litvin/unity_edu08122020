﻿using UnityEngine;
using UnityEngine.UI;

public class ItemInventar : MonoBehaviour
{
    public string nameItem;
    public int countItem;
    public Image image;
    public int idCell;
    private Text textCount;
    private MouseInventar mouse;
    

    void Start()
    {
        mouse = FindObjectOfType<MouseInventar>();
        textCount = GetComponentInChildren<Text>();
        image = GetComponent<Image>();
        if(image.sprite)
        {
            textCount.text = countItem.ToString();
        }
        else
        {
            image.color = new Color(1, 1, 1, 0);
            textCount.text = "";
            countItem = 0;
        }
    }

    public void BeginDrag()
    {
        print("BeginDrag: "+name);
        if(image.sprite && !mouse.image.sprite)
        {
            CellToMouse();
        }
    }

    public void EndDrag()
    {
        print("EndDrag: " + name);
        if(mouse.image.sprite)
        {
            MouseToCell();

        }
    }

    public void Drop()
    {
        print("Drop: " + name);
        if(image.sprite)
        {
            SwapMouseCell();
        }
        else if(mouse.image.sprite)
        {
            MouseToCell();
        }
    }

    public void SetCount(int count)
    {
        countItem = count;
        textCount.text = count == 0 ? "" : count.ToString();
    }

    private void CellToMouse()
    {
        mouse.idCellMuve = idCell;
        mouse.image.sprite = image.sprite;
        mouse.image.color = Color.white;
        mouse.SetCount(countItem);
        image.color = new Color(1, 1, 1, 0);
        image.sprite = null;
        SetCount(0);
        mouse.nameItem = nameItem;
    }

    private void MouseToCell()
    {
        image.sprite = mouse.image.sprite;
        image.color = Color.white;
        mouse.image.color = new Color(1, 1, 1, 0);
        mouse.image.sprite = null;
        SetCount(mouse.count);
        mouse.SetCount(0);
        nameItem = mouse.nameItem;
    }

    private void SwapMouseCell()
    {
        Sprite tempSprite = image.sprite;
        image.sprite = mouse.image.sprite;
        mouse.image.sprite = tempSprite;
        int tempCount = countItem;
        SetCount(mouse.count);
        mouse.SetCount(tempCount);
        string tempName = nameItem;
        nameItem = mouse.nameItem;
        mouse.nameItem = tempName;
    }

}
