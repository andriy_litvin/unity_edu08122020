﻿using System.IO;
using UnityEngine;


public class LoadSave1 : MonoBehaviour
{
    public SpriteRenderer sprite;
    public Transform cube;
    public Collider[] objsColl;
    public int score;
    public string name;

    public Transform objSave;

    void Start()
    {
        sprite = Resources.Load<SpriteRenderer>("test/Sprite");
        cube = Resources.Load<Transform>("Cube");
        objsColl = Resources.LoadAll<Collider>("test");

        LoadFile();
        score = PlayerPrefs.GetInt("score");
        name = PlayerPrefs.GetString("name");

        string loadPos = PlayerPrefs.GetString("pos");
        if(loadPos.Length!=0)
        {
            string[] arrayPos = loadPos.Split(';');
            Vector3 vectorPos = Vector3.zero;

            for (int i = 0; i < 3; i++)
            {
                vectorPos[i] = float.Parse(arrayPos[i]);
            }
            objSave.position = vectorPos;
        }

    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("score", score);
        PlayerPrefs.SetString("name", name);

        string savePos = "";
        for (int i = 0; i < 3; i++)
        {
            savePos += objSave.position[i] +";" ;
        }
        // 1;2;3;
        savePos = savePos.Remove(savePos.Length - 1);
        // 1;2;3
        PlayerPrefs.SetString("pos", savePos);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LoadFile()
    {
        string fileStr, path;
#if UNITY_ANDROID && !UNITY_EDITOR
        path = Path.Combine(Application.streamingAssetsPath, "Test.txt");
        WWW read = new WWW(path);
        while(!read.isDone)
        {
            fileStr = read.text;
        }
#endif

#if UNITY_EDITOR
        fileStr = File.ReadAllText(Application.streamingAssetsPath + "/Test.txt");
#endif
        //print(fileStr);
    }
}
