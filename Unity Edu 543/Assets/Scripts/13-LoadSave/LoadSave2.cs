﻿using System.IO;
using UnityEngine;

public class LoadSave2 : MonoBehaviour
{
    public Save save;
    string path;

    void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        path = Path.Combine(Application.persistentDataPath, "Save.json");
#else
        path = Path.Combine(Application.dataPath, "Save.json");
#endif
        if(File.Exists(path))
        {
            save = JsonUtility.FromJson<Save>(File.ReadAllText(path));
        }
        else
        {
            print("not save file");
        }

    }

    private void OnApplicationQuit()
    {
        File.WriteAllText(path, JsonUtility.ToJson(save)); 
    }


}

[System.Serializable]
public class Save
{
    public string name;
    public int level;
    public int score;
    public int hp;
    public Vector3 pos;
    public Vector3[] checkPoints;
}
