﻿using UnityEngine;

public class BonusManager : MonoBehaviour
{
    public int countStartBonus = 5;
    public Transform bonusPref;
    public Transform[] bonusPoints;
    public LayerMask layerSensety;
    private int countBonus = 0;

    private void Awake()
    {
        Messenger.AddListener("pickCointEvent", OnPickCoint);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener("pickCointEvent", OnPickCoint);
    }

    void Start()
    {
        countBonus = FindObjectsOfType<CoinsMario>().Length;

        if (bonusPoints.Length < countStartBonus)
        {
            countStartBonus = bonusPoints.Length;
            print("Відкореговано кількість бонусів");
        }
        CreateBonus(countStartBonus); 
    }

    void OnPickCoint()
    {
        countBonus--;
        if (countBonus <= 2)
        {
            CreateBonus(1);
        }
    }

    void CreateBonus(int count)
    {
        int countTempBonus = 0;
        while (countTempBonus < count)
        {
            Vector2 pos = bonusPoints[Random.Range(0, bonusPoints.Length)].position;
            if (!Physics2D.OverlapCircle(pos, 0.2f, layerSensety))
            {       
                Instantiate(bonusPref, pos, Quaternion.identity, transform);
                countBonus++;
                countTempBonus++;
            }
        }
    }

}
