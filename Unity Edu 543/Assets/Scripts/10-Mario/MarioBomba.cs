﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarioBomba : MonoBehaviour
{
    public float timer = 2;
    private SpriteRenderer _spriteRenderer;

    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(Anim());
        StartCoroutine(Boom());
    }

    IEnumerator Anim()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.3f);
            _spriteRenderer.color = Color.red;
            yield return new WaitForSeconds(0.3f);
            _spriteRenderer.color = Color.white;
        }
    }

    IEnumerator Boom()
    {
        yield return new WaitForSeconds(timer);
        Collider2D[] colls = Physics2D.OverlapCircleAll(transform.position, 1);
        foreach (var item in colls)
        {
            if (item.tag == "blockWall")
            {
                Destroy(item.gameObject);
            }     
        }
        Destroy(gameObject);
    }

}
