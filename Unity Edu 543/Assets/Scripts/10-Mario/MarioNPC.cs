﻿using UnityEngine;
using System.Collections.Generic;

public class MarioNPC : MonoBehaviour
{
    public float speed = 1;
    public int hp = 20;
    public LayerMask layerWall;
    private Rigidbody2D _rigidbody2D;
    private Transform _transform;
    private Vector2 direction;

    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _transform = transform;
        direction = Vector2.up;
    }

    void FixedUpdate()
    {
        if (direction == Vector2.up && Physics2D.OverlapCircle(_transform.position + Vector3.up * 0.45f, 0.1f, layerWall))
        {
            Turn();
        }
        else if (direction == Vector2.down && Physics2D.OverlapCircle(_transform.position + Vector3.down * 0.45f, 0.1f, layerWall))
        {
            Turn();
        }
        else if (direction == Vector2.left && Physics2D.OverlapCircle(_transform.position + Vector3.left * 0.45f, 0.1f, layerWall))
        {
            Turn();
        }
        else if (direction == Vector2.right && Physics2D.OverlapCircle(_transform.position + Vector3.right * 0.45f, 0.1f, layerWall))
        {
            Turn();
        }

        _rigidbody2D.velocity = direction * speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "bulet")
        {
            _rigidbody2D.velocity = Vector2.zero;
            DamageToNPC(7);
        }
    }

    void DamageToNPC(int damage)
    {
        hp -= damage;
        if (hp <= 0)
        {

            Destroy(gameObject);
        }
    }

    void Turn()
    {
        List<Vector2> dir = new List<Vector2> { Vector2.up, Vector2.down, Vector2.left, Vector2.right };
        dir.Remove(direction);

        for (int i = 0; i < dir.Count; i++)
        {
            int randDir = Random.Range(0, dir.Count);
            if (Physics2D.OverlapCircle(_transform.position + (Vector3)dir[randDir] * 0.45f, 0.1f, layerWall))
            {
                dir.RemoveAt(randDir);
                i--;
            }
            else
            {
                direction = dir[randDir];
                return;
            }
        }

        if(dir.Count==0)
        {
            Debug.LogError("Not direction");
            this.enabled = false;
        }
    }
}
