﻿using UnityEngine;

public class CoinsMario : MonoBehaviour
{
          
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<MarioController>().AddScore(2);
            Messenger.Broadcast("pickCointEvent");
            Destroy(gameObject);
        }
    }
}
