﻿using UnityEngine;

public class MarioController : MonoBehaviour
{
    public float speed = 1;
    public Rigidbody2D buletPref;
    public Transform bombaPref;
    public float forseBulet;
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private Transform _transform;
    private Vector2 direction;
    private bool isRight;
    private int score = 0;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        direction = Vector2.right;
        isRight = true;
        _transform = transform;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            direction = Vector2.left;
            Flip();
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            direction = Vector2.right;
            Flip();
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            direction = Vector2.up;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            direction = Vector2.down;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Rigidbody2D temp = Instantiate(buletPref, _transform.position, Quaternion.identity);
            temp.AddForce(direction * forseBulet);
            if (direction == Vector2.down || direction == Vector2.up)
            {
                temp.transform.Rotate(0, 0, 90);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            Instantiate(bombaPref, _transform.position, Quaternion.identity);
        }
    }

    private void FixedUpdate()
    {
        //rb.velocity = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * speed;

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector2(Input.GetAxis("Horizontal"), 0) * speed;
        }
        else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
        {
            rb.velocity = new Vector2(0, Input.GetAxis("Vertical")) * speed;
        }

    }

    void Flip()
    {
        if (direction == Vector2.left && isRight)
        {
            sr.flipX = true;
            isRight = false;
        }
        else if (direction == Vector2.right && !isRight)
        {
            sr.flipX = false;
            isRight = true;
        }
    }
       
    public void AddScore(int addScore)
    {
        score += addScore;
        print("Score: " + score);
    }
}
