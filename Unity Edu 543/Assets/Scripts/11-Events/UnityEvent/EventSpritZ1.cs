﻿using UnityEngine.Events;
using UnityEngine;

public class EventSpritZ1 : MonoBehaviour
{
    [SerializeField]
    private Sprite[] sprites;
    [SerializeField]
    private SpriteEvent spriteEvent;
    
    private void Start()
    {
        ListenerSprZ1[] listenerSpr = GetComponentsInChildren<ListenerSprZ1>();
        foreach(var item in listenerSpr)
        {
            spriteEvent.AddListener(item.SetSprite);
        }
    }

    private void OnMouseDown()
    {
        spriteEvent.Invoke(sprites[Random.Range(0, sprites.Length)]);  
    }
}

[System.Serializable]
public class SpriteEvent: UnityEvent<Sprite>
{
}