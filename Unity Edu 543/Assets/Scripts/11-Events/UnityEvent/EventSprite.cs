﻿using UnityEngine.Events;
using UnityEngine;

public class EventSprite : MonoBehaviour
{
    [SerializeField]
    private UnityEvent UpdateObjEvent;

    private void OnMouseDown()
    {
        UpdateObjEvent.Invoke();
    }
}
