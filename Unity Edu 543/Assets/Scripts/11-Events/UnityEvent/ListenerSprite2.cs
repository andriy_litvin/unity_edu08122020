﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListenerSprite2 : MonoBehaviour
{
    [SerializeField]
    private Sprite[] spritesMobs;
    private SpriteRenderer _spriteRenderer;

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void MobEvent()
    {
        _spriteRenderer.sprite = spritesMobs[Random.Range(0, spritesMobs.Length)];
    }
}
