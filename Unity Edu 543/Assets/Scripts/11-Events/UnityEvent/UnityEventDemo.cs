﻿using UnityEngine.Events;
using UnityEngine;

public class UnityEventDemo : MonoBehaviour
{
    [SerializeField]
    private UnityEvent demoEvent;
    public StrEvent demoStrEvent;

    void Start()
    {
        demoEvent.AddListener(Demo); 
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            //demoEvent.Invoke();
            demoStrEvent.Invoke("test " + Random.Range(10, 100));
        }
    }

    public void Demo()
    {
        print("unity event demo");
    }

    public void Demo(string str)
    {
        print("unity event demo: "+str);
    }
}

[System.Serializable]
public class StrEvent:UnityEvent<string>
{
}