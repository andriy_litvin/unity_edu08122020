﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListenerEvent : MonoBehaviour
{
    void Awake()
    {
        Messenger.AddListener("myEvent", OnMyEvent);
        Messenger<int>.AddListener("myIntEvent", OnMyIntEvent);
        Messenger.AddListener<string>("myStrEvent", OnMyStrEvent);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener("myEvent", OnMyEvent);
        Messenger<int>.RemoveListener("myIntEvent", OnMyIntEvent);
        Messenger.RemoveListener<string>("myStrEvent", OnMyStrEvent);
    }

    void OnMyEvent()
    {
        print("my event: " + name);
    }

    void OnMyIntEvent(int a)
    {
        print($"my int event: {name} * {a}");
    }

    string OnMyStrEvent()
    {
        return $"my str event-> {name}";
    }
}
