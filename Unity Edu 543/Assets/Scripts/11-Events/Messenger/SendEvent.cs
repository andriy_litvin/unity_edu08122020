﻿using UnityEngine;

public class SendEvent : MonoBehaviour
{         
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Messenger.Broadcast("myEvent");
        }
        else if (Input.GetMouseButtonDown(1))
        {
            Messenger<int>.Broadcast("myIntEvent", Random.Range(1, 10));
        }
        else if (Input.GetMouseButtonDown(2))
        {
            Messenger.Broadcast<string>("myStrEvent",RezulMyStrEvent);
        }        
    }

    void RezulMyStrEvent(string str)
    {
        print($"{name}:  {str}");
    }
}
