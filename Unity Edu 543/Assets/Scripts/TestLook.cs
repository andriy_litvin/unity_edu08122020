﻿using UnityEngine;

public class TestLook : MonoBehaviour
{
    Transform _transform;
    Rigidbody2D rb;

    void Start()
    {
        _transform = transform;
        rb = GetComponent<Rigidbody2D>();        
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 0;
            print(pos);
            pos = pos - _transform.position;
            print(pos);
            _transform.rotation = Quaternion.FromToRotation(Vector3.right, pos);
            rb.velocity = Vector2.zero;
            rb.AddForce(_transform.TransformDirection(Vector3.right) * 30); 
        }
    }
}
