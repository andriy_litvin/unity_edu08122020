﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class SavePlatformer : MonoBehaviour
{
    public SaveGame save;
    public Transform NPCPref;    

    string path;

    void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        path = Path.Combine(Application.persistentDataPath, "Save", "SavePlatformer.json");
#else
        path = Path.Combine(Application.dataPath, "Save", "SavePlatformer.json");
#endif
        if (File.Exists(path))
        {
            save = JsonUtility.FromJson<SaveGame>(File.ReadAllText(path));
            LoadInfo();
        }
        else
        {
            print("not save file");
        }

    }

    private void OnApplicationQuit()
    {
        SaveInfo();
        File.WriteAllText(path, JsonUtility.ToJson(save));
    }

    void SaveInfo()
    {
        save.userName = PlayerParams.singletone.userName;
        save.level = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
        save.hp = PlayerParams.singletone.hp;
        save.score = PlayerParams.singletone.Score;
        save.playerPosition = PlayerParams.singletone.transform.position;

        CatNPC[] NPCs = FindObjectsOfType<CatNPC>();
        save.NPCsPosition = new Vector3[NPCs.Length];
        for(int i=0; i<NPCs.Length; i++)
        {
            save.NPCsPosition[i] = NPCs[i].transform.position;
        }

        BonusPlatformer[] bonuses = FindObjectsOfType<BonusPlatformer>();
        save.bonuses5Position.Clear();
        save.bonuses10Position.Clear();
        for (int i = 0; i < bonuses.Length; i++)
        {
            if(bonuses[i].addScore==5)
            {
                save.bonuses5Position.Add(bonuses[i].transform.position);
            }
            else
            {
                save.bonuses10Position.Add(bonuses[i].transform.position);
            }
        }

    }

    void LoadInfo()
    {
        PlayerParams.singletone.userName = save.userName;
        PlayerParams.singletone.level = save.level;
        PlayerParams.singletone.hp = save.hp;
        PlayerParams.singletone.Score = save.score;
        PlayerParams.singletone.transform.position = save.playerPosition;

        CatNPC[] NPCs = FindObjectsOfType<CatNPC>();        
        for(int i=0; i<NPCs.Length; i++)
        {
            if(i<save.NPCsPosition.Length)
            {
                NPCs[i].transform.position = save.NPCsPosition[i];
            }
            else
            {
                Destroy(NPCs[i].gameObject);
            }
        }

        if(NPCs.Length<save.NPCsPosition.Length)
        {
            for (int i = NPCs.Length; i > save.NPCsPosition.Length; i++)
            {
                Instantiate(NPCPref, save.NPCsPosition[i], Quaternion.identity);   
            }
        }

        List<BonusPlatformer> bonuses = new List<BonusPlatformer>(FindObjectsOfType<BonusPlatformer>());
        for (int i = 0; i < save.bonuses5Position.Count; i++)
        {
            for (int j = 0; j < bonuses.Count; j++)
            {
                if(bonuses[j].transform.position == save.bonuses5Position[i])
                {
                    bonuses.RemoveAt(j);
                    break;
                }
            }
        }

        for (int i = 0; i < save.bonuses10Position.Count; i++)
        {
            for (int j = 0; j < bonuses.Count; j++)
            {
                if (bonuses[j].transform.position == save.bonuses10Position[i])
                {
                    bonuses.RemoveAt(j);
                    break;
                }
            }
        }

        foreach (var item in bonuses)
        {
            Destroy(item.gameObject);
        }



    }
}

[System.Serializable]
public class SaveGame
{
    public string userName;
    public int level;
    public int hp;
    public int score;
    public Vector3 playerPosition;
    public Vector3[] NPCsPosition;
    public List<Vector3> bonuses5Position;
    public List<Vector3> bonuses10Position;
}
