﻿using UnityEngine;

public class CatAnimation : MonoBehaviour
{
    public Transform sensorGround;
    public LayerMask layerGround;
    private Rigidbody2D _rigidbody2D;
    private Animator _animator;
    private SpriteRenderer _spriteRenderer;
    private bool isGround = false;
    private float speedX = 0;
    private bool isRight = true;

    public bool IsGround { get { return isGround; } }
    public bool IsRight { get { return isRight; } }

    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        
    }

    void FixedUpdate()
    {
        isGround = Physics2D.OverlapCircle(sensorGround.position, 0.2f, layerGround);
        _animator.SetBool("ground", isGround);
        _animator.SetFloat("speedY", _rigidbody2D.velocity.y);
        speedX = _rigidbody2D.velocity.x;
        _animator.SetFloat("speedX", Mathf.Abs(speedX));
        
        if(speedX>0.1f && !isRight)
        {
            _spriteRenderer.flipX = false;
            isRight = true;
        }
        else if (speedX < -0.1f && isRight)
        {
            _spriteRenderer.flipX = true;
            isRight = false;
        }
    }
}
