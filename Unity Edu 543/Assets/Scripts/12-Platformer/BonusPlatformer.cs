﻿using UnityEngine;

public class BonusPlatformer : MonoBehaviour
{
    public int addScore=5;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            //collision.GetComponent<CatPlayer>().AddScore(addScore);
            PlayerParams.singletone.Score += addScore;
            if(gameObject) Destroy(gameObject);
        }
    }
}
 
