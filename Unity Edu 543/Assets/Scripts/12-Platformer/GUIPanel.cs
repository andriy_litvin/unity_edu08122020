﻿using UnityEngine;

[ExecuteInEditMode]
public class GUIPanel : MonoBehaviour
{
    private string strScore="Score: 0";

    public int score
    {
        set
        {
            strScore = "Score: " + value;
        }
    }

    private void OnGUI()
    {
        GUI.BeginGroup(new Rect(10, 10, 200, 40));
        GUI.Box(new Rect(0, 0, 150, 40), "");
        GUI.Label(new Rect(20, 10, 140, 20), strScore);
        GUI.EndGroup();
    }
}
