﻿using UnityEngine;

public class PlayerParams : MonoBehaviour
{
    public string userName;
    public int level;
    public int hp;
    //private GUIPanel panel;
    private UIController _uIControl;
    private int _score; 

    public int Score   
    {
        get
        {
            return _score;
        }
        set
        {
            //print($"Score {value} (+{value - _score})");
            _score = value;
            //panel.score = _score;
            _uIControl.Score = _score;
        }

    }

    /* лінивий Сінглетон
    public static PlayerParams singletone { get; private set; }

    private void Awake()
    {
        singletone = this;
    }*/

    /* стандартний Сінглетон
    private static PlayerParams _singletone;
    public static PlayerParams singletone
    {
        get
        {
            if(!_singletone)
            {
                _singletone = FindObjectOfType<PlayerParams>();
            }
            return _singletone;
        }
    }
    */

    // стійкий Сінглетон
    private static PlayerParams _singletone;

    public static PlayerParams singletone
    {
        get
        {
            if (!_singletone)
            {
                _singletone = FindObjectOfType<PlayerParams>();
                DontDestroyOnLoad(_singletone.gameObject);
            }
            return _singletone;
        }
    }

    private void Awake()
    {
        if(!_singletone)
        {
            _singletone = this;
            DontDestroyOnLoad(_singletone.gameObject);
        }
        else if(this != _singletone)
        {
            Destroy(this.gameObject);
        }

        //panel = FindObjectOfType<GUIPanel>();
        _uIControl = FindObjectOfType<UIController>();
    }
}
