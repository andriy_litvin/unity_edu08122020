﻿using UnityEngine;

public class ParalaxController : MonoBehaviour
{
    public float dumping = 2;
    private Transform _transform;
    private ParallaxTest _parallax;
    private Transform _transformCamera;
    private Vector3 newCamPos; // нова позиція камери для проміжних обчислень
    private float posX; // величина зміщення, що передається в паралакс

    void Start()
    {
        _transform = transform;
        _transformCamera = Camera.main.transform;
        _parallax = _transformCamera.GetComponentInChildren<ParallaxTest>();
        newCamPos = _transformCamera.position;
        posX = _transformCamera.position.x;
    }

    void Update()
    {
        newCamPos.x = _transform.position.x;
        _transformCamera.position = Vector3.Lerp(_transformCamera.position, newCamPos, dumping * Time.deltaTime);
        _parallax.run = (_transformCamera.position.x - posX) * 20;
        posX = _transformCamera.position.x;
    }
}
