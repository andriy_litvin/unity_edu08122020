﻿using UnityEngine;

public class ParalaxController2 : MonoBehaviour
{
    public float dumping = 2;
    public float forwardCamX = 3;
    private Transform _transform;
    private ParallaxTest _parallax;
    private Transform _transformCamera;
    private CatAnimation _catAnimation;
    private Vector3 newCamPos; // нова позиція камери для проміжних обчислень
    private float posX, posY; // величина зміщення, що передається в паралакс
    private Vector3 deltaCamPos; // різниця на зміщення камери відносно гравця

    void Start()
    {
               
        _catAnimation = GetComponent<CatAnimation>();
        InitParalax();
    }

    private void OnLevelWasLoaded(int level)
    {
        print("scene " + level);
        InitParalax();
    }

    void Update()
    {
        newCamPos = _transform.position + deltaCamPos;
        if(_catAnimation.IsRight)
        {
            newCamPos.x += forwardCamX;
        }
        else
        {
            newCamPos.x -= forwardCamX;
        }
        _transformCamera.position = Vector3.Lerp(_transformCamera.position, newCamPos, dumping * Time.deltaTime);
        _parallax.run = (_transformCamera.position.x - posX) * 20;
        posX = _transformCamera.position.x;
        _parallax.jump = _transformCamera.position.y - posY;
        posY = _transformCamera.position.y;
    }

    private void InitParalax()
    {
        _transform = transform;
        _transformCamera = Camera.main.transform;
        _parallax = _transformCamera.GetComponentInChildren<ParallaxTest>();
        newCamPos = _transformCamera.position;
        posX = _transformCamera.position.x;
        posY = _transformCamera.position.y;
        deltaCamPos = _transformCamera.position - _transform.position;
    }
}
