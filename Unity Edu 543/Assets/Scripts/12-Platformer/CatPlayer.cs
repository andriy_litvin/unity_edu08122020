﻿using UnityEngine;

public class CatPlayer : MonoBehaviour
{
    public float speed = 1;
    public float addForseSuriken = 200;
    [SerializeField]
    private Rigidbody2D surikenPref;
    private Rigidbody2D _rigidbody2D;
    private float move;
    private CatAnimation _catAnimation;    
    private Transform _transform;

    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _catAnimation = GetComponent<CatAnimation>();
        _transform = transform;
    }

    private void OnLevelWasLoaded(int level)
    {
        _transform.position = GameObject.FindGameObjectWithTag("Respawn").transform.position;
    }

    void Update()
    {
        move = Input.GetAxis("Horizontal");
        _rigidbody2D.velocity = new Vector2(move * speed, _rigidbody2D.velocity.y);

        if(Input.GetKeyDown(KeyCode.Space)&& _catAnimation.IsGround)
        {
            _rigidbody2D.AddForce(new Vector2(0, 2000));
        }
        else if(Input.GetKeyDown(KeyCode.Return))
        {
            Rigidbody2D temp = Instantiate(surikenPref, _transform.position + new Vector3(0, 0.2f, 0), Quaternion.identity);
            if(_catAnimation.IsRight)
            {
                temp.AddForce(new Vector2(addForseSuriken, 0));
            }
            else
            {
                temp.AddForce(new Vector2(-addForseSuriken, 0));
            }
        }
    }        
}
