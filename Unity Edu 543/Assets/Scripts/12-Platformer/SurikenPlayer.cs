﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurikenPlayer : MonoBehaviour
{
    private Transform transformChild;

    void Start()
    {
        transformChild = GetComponentsInChildren<Transform>()[1];
        StartCoroutine(Animation());
    }
      

    IEnumerator Animation()
    {
        while(true)
        {
            transformChild.Rotate(0, 0, -10);
            yield return null;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
