﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private Text textScore;

    public int Score
    {
        set
        {
            textScore.text = "Score: " + value;
        }
    }
    
}
