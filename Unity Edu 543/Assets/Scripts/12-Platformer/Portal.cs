﻿using System.Collections;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public Transform portalAction;
    private bool isAction=false;

    void Start()
    {
        portalAction.gameObject.SetActive(false); 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            portalAction.gameObject.SetActive(true);
            StartCoroutine(Anim());
            isAction = true;
        }
    }

    private void Update()
    {
        if(isAction && Input.GetKeyDown(KeyCode.R))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        }
    }


    IEnumerator Anim()
    {
        while(true)
        {
            portalAction.Rotate(0, 0, 30 * Time.deltaTime);
            yield return null;
        }
    }

}
