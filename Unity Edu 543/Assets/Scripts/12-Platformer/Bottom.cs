﻿using UnityEngine;

public class Bottom : MonoBehaviour
{
    public Transform startPosition;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            collision.transform.position = startPosition.position;
        }
        else
        {
            Destroy(collision.gameObject);
        }
    }
}
