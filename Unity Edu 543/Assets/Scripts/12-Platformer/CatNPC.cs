﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CatNPC : MonoBehaviour
{
    public float speed = 1;
    public int hp = 20;
    public float addForseSuriken = 200;
    public float deltaTimeShot = 1;
    [SerializeField]
    private LayerMask layerPlatform;
    [SerializeField]
    private Transform sensorPlatformRight;
    [SerializeField]
    private Transform sensorPlatformLeft;
    [SerializeField]
    private Rigidbody2D surikenPref;
    [SerializeField]
    private LayerMask layerPlayer;
    private Image imageHP;
    private Rigidbody2D _rigidbody2D;
    private float move; // напрямок руху
    private CatAnimation _catAnimation;    
    private Transform _transform;
    private Coroutine corFindTarget;
    private float fullHP;

    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _catAnimation = GetComponent<CatAnimation>();
        imageHP = GetComponentInChildren<Image>();
        _transform = transform;
        move = _catAnimation.IsRight ? 1 : -1;
        fullHP = hp;

    }
        
    void Update()
    {
        if(_catAnimation.IsRight && !Physics2D.OverlapCircle(sensorPlatformRight.position, 0.2f, layerPlatform))
        {
            Turn();     
        }
        else if(!_catAnimation.IsRight && !Physics2D.OverlapCircle(sensorPlatformLeft.position, 0.2f, layerPlatform))
        {
            Turn();
        }

        _rigidbody2D.velocity = new Vector2(move * speed, _rigidbody2D.velocity.y);
    }

    private void OnBecameVisible()
    {
        corFindTarget = StartCoroutine(FindTarget());  
    }

    private void OnBecameInvisible()
    {
        StopCoroutine(corFindTarget); 
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag=="bulet")
        {
            print("-hp");
            hp -= 5;
            imageHP.fillAmount = hp / fullHP;
            if(hp<=0)
            {
                Destroy(gameObject);
                PlayerParams.singletone.Score += 10;
            }
        }
    }

    IEnumerator FindTarget()
    {
        while(true)
        {
            if(Physics2D.Raycast(_transform.position, Vector2.right*move, 50, layerPlayer))
            {
                Rigidbody2D temp = Instantiate(surikenPref, _transform.position + new Vector3(0, 0.2f, 0), Quaternion.identity);
                temp.AddForce(new Vector2(addForseSuriken * move, 0));
                yield return new WaitForSeconds(deltaTimeShot);
            }
            yield return null;
        }
    }

    void Turn()
    {
        move *= -1;

    }


}
