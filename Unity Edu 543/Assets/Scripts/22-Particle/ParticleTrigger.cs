﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ParticleTrigger : MonoBehaviour
{
    ParticleSystem ps;
    List<ParticleSystem.Particle> enterParticle = new List<ParticleSystem.Particle>();

    private void OnEnable()
    {
        ps = GetComponent<ParticleSystem>();
    }

    private void OnParticleTrigger()
    {
        int countPar = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, enterParticle);
        for (int i = 0; i < countPar; i++)
        {
            ParticleSystem.Particle tempPar = enterParticle[i];
            tempPar.startColor = Color.red;
            enterParticle[i] = tempPar;
        }
        ps.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, enterParticle);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            var trig = ps.trigger;
            ParticleSystem.TriggerModule trigger = trig;
            trigger.exit = ParticleSystemOverlapAction.Kill;
            trig.exit = trigger.exit;
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            var trig = ps.trigger;
            ParticleSystem.TriggerModule trigger = trig;
            trigger.exit = ParticleSystemOverlapAction.Ignore;
            trig.exit = trigger.exit;
        }
    }
}

