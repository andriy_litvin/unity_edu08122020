﻿using UnityEngine;
using System.Collections.Generic;

public class ParticleExplousion : MonoBehaviour
{
    ParticleSystem ps;
    List<ParticleCollisionEvent> collEvent = new List<ParticleCollisionEvent>();

    void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.tag == "wall")
        {
            int countColl = ps.GetCollisionEvents(other, collEvent);
            Rigidbody rb = other.GetComponent<Rigidbody>();
            foreach (var item in collEvent)
            {
                rb.AddForce(item.velocity * 40);
            }
        }
    }
}
