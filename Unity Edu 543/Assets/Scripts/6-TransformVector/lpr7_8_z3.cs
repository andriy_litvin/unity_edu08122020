﻿using UnityEngine;

public class lpr7_8_z3 : MonoBehaviour
{
    void Start()
    {
        Transform obj1 = CreateObj(Vector3.zero, "obj 1", null);
        Transform[] objs2 = new Transform[3];
        for (int i = 0; i < 3; i++)
        {
            objs2[i] = CreateObj(new Vector3(-8 + 8 * i, -2, 0), $"obj 1.{i + 1}", obj1); 
        }

    }

    Transform CreateObj(Vector3 pos, string name, Transform parent)
    {
        Transform temp = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
        temp.position = pos;
        temp.name = name;
        temp.parent = parent;
        return temp;
    }
    
}
