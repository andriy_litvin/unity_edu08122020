﻿using UnityEngine;

public class TransformVectorTest : MonoBehaviour
{
    public Vector2 a;
    public Vector2Int v2i;
    public Vector3 v3;
    public Vector4 v4;

    public Transform obj1;
    public Transform obj2;
    public float speedRot = 1;
    public float speedMuve = 1;

    void Start()
    {
        /*a = Vector2.one;
        v3 = Vector3.right;
        v4 = new Vector4(1, 2, 3, 4);
        Vector2 b = new Vector2(5, 7);

        print(Vector2.Distance(a, b));

        for (int t = 0; t <= 10; t++)
        {
            print(Vector2.Lerp(a, b, t / 10f));
        }

        while (Vector2.Distance(a, b) > 0.01)
        {
            a = Vector2.MoveTowards(a, b, 0.5f);
            print(a);
        }    */
    }

    void Update()
    {
        /*obj1.Rotate(0, speedRot*Time.deltaTime, 0);
        obj2.Translate(speedMuve*Time.deltaTime, 0, 0); */

        //obj2.RotateAround(obj1.position, Vector3.up, speedRot * Time.deltaTime);

        obj1.LookAt(obj2);
    }
}
