﻿using UnityEngine;

public class PatrulTest : MonoBehaviour
{
    public Transform obj;
    public float speed = 1;
    public float stopDis = 2;
    public Transform[] patrul;
    public int nextPointId=0;

    private Transform _transform; 

    void Start()
    {
        _transform = transform;
        //FindObjectOfType<PatrulTest>().transform;
    }

    void Update()
    {
        // патрульний (жовтий)
        if (Vector3.Distance(_transform.position, patrul[nextPointId].position) > 0.1)
        {
            _transform.position = Vector3.MoveTowards(_transform.position, patrul[nextPointId].position,
                                                    speed * Time.deltaTime);
        }
        else
        {
            nextPointId = ++nextPointId % patrul.Length;
            // 3 % 5 = 3
            // 5 % 5 = 0
        }

        // переслідувач (червоний)
        obj.LookAt(_transform);
        if (Vector3.Distance(obj.position, _transform.position) > stopDis)
        {
            obj.Translate(0, 0, speed * Time.deltaTime, Space.Self);       
        }


    }
}
