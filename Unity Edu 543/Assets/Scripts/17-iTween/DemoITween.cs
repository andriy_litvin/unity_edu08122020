﻿using UnityEngine;

public class DemoITween : MonoBehaviour
{
    public Transform point;
    public Transform[] points;

    void Start()
    {
        //iTween.MoveAdd(gameObject, new Vector3(3, 0, 0), 2);
        //iTween.MoveTo(gameObject, iTween.Hash("x", 4, "time", 4, 
        //            "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong));

        //iTween.MoveFrom(gameObject, iTween.Hash("position", point, "time", 3, "delay", 1));
        //iTween.MoveTo(gameObject, iTween.Hash("path", points, "speed", 2, "delay", 1, 
        //            "easetype",iTween.EaseType.linear, "orienttopath", true));
        //iTween.RotateTo(gameObject, iTween.Hash("y", 90, "time", 2, "looptype", iTween.LoopType.pingPong));
        //iTween.ShakePosition(gameObject, iTween.Hash("x", 2, "time", 3, "delay", 1));
        //iTween.PunchPosition(gameObject, iTween.Hash("x", 2, "time", 3, "delay", 1));

        iTween.MoveAdd(gameObject, iTween.Hash("x", 4, "time", 3, "delay", 1, "onstart", "AnimStart"));  // 3 c.
        iTween.RotateAdd(gameObject, iTween.Hash("y", 180, "time", 5, "delay", 3));  // 2 c.
        iTween.ColorTo(gameObject, iTween.Hash("color", Color.yellow, "time", 2, "delay", 5,
                    "oncomplete", "AnimFinish", "oncompleteparams", 5)); // 2 c.
    }

    private void OnDrawGizmos()
    {
        iTween.DrawPath(points, Color.green);
    }

    void AnimStart()
    {
        print("start");
    }

    void AnimFinish(float time)
    {
        print("finish "+time);
        print(Time.time);
    }
}
