﻿using UnityEngine;

public class ParallaxTest : MonoBehaviour
{
    public Transform[] parArray;
    public float run;
    public float jump;
    private int countElem;    
    private float[] speedArray = { 0.01f, 0.02f, 0.04f};
    private float[] speedArrayY = { 0, 0.08f, 0.16f };

    private void Start()
    {
        countElem = parArray.Length;
    }  

    void Update()
    {
        for (int i = 0; i < countElem; i++)
        {
            parArray[i].localPosition -= new Vector3(speedArray[i/2]*run, speedArrayY[i / 2] * jump, 0);

            if (parArray[i].localPosition.x < -25)
            {
                parArray[i].localPosition += new Vector3(50, 0, 0);
            }
            else if (parArray[i].localPosition.x > 25)
            {
                parArray[i].localPosition -= new Vector3(50, 0, 0);
            }
        }

       
    }
}
