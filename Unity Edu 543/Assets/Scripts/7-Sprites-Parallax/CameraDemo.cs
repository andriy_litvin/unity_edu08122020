﻿using UnityEngine;

public class CameraDemo : MonoBehaviour
{
    public Transform[] path;
    public float speed=1;
    private ParallaxTest parallax;
    private Transform _transform;
    private int idNextPoint = 1;
    float lastPosX;

    void Start()
    {
        parallax = GetComponentInChildren<ParallaxTest>();
        _transform = transform;
        lastPosX = _transform.position.x;
    }

    void Update()
    {
        if (Vector3.Distance(_transform.position, path[idNextPoint].position) > 0.01)
        {
            _transform.position = Vector3.MoveTowards(_transform.position, path[idNextPoint].position,
                                    speed * Time.deltaTime);
        }
        else
        {
            idNextPoint = ++idNextPoint % path.Length;
            //  2 % 2 = 0
            //  1 % 2 = 1            
            //  2 % 2 = 0
        }
        parallax.run = (_transform.position.x - lastPosX) * 10;
        lastPosX = _transform.position.x;
    }
}