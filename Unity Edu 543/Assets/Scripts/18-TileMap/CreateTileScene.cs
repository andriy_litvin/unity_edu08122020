﻿using UnityEngine.Tilemaps;
using UnityEngine;

public class CreateTileScene : MonoBehaviour
{
    public Tilemap map;
    // 0 - вода, 1 - трава, 2 - степ, 3 - пісок
    public TileBase[] tiles;

    void Start()
    {
        int[,] idTiles = new int[50, 50];
        for (int i = 0; i < 50; i++)
        {
            for (int j = 0; j < 50; j++)
            {
                idTiles[i, j] = Random.Range(0,tiles.Length);
            }
        }

        for (int y = 0; y < 50; y++)
        {
            for (int x = 0; x < 50; x++)
            {
                map.SetTile(new Vector3Int(y, x + 20, 0), tiles[idTiles[y, x]]);
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
