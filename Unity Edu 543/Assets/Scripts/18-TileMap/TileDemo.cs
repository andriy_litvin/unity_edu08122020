﻿using UnityEngine.Tilemaps;
using UnityEngine;

public class TileDemo : MonoBehaviour
{
    public Tilemap map;
    public TileBase[] tilesSet;

    void Update()
    {
        for (int i = 0; i < 3; i++)
        {
            if (Input.GetMouseButtonDown(i))
            {
                SetTileOnClickMouse(tilesSet[i]);
            }
        }

    }

    void SetTileOnClickMouse(TileBase tile)
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int cellPos = map.WorldToCell(worldPoint);
        print(cellPos);
        cellPos.z = 0;
        map.SetTile(cellPos, tile);
    }
}
