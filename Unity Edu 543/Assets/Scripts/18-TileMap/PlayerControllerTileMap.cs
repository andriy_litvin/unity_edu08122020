﻿using UnityEngine;

public class PlayerControllerTileMap : MonoBehaviour
{
    public float speed = 3;
    public float dumping = 2;
    private Transform _transform;
    private Rigidbody2D _rigidbody2d;
    private Transform transformCamera;
    private Vector3 newPosCam;

    void Start()
    {
        _transform = transform;
        _rigidbody2d = GetComponent<Rigidbody2D>();
        transformCamera = Camera.main.transform;
    }

    void Update()
    {
        _rigidbody2d.velocity = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * speed;
        newPosCam = _transform.position;
        newPosCam.z = -10;
        transformCamera.position = Vector3.Lerp(transformCamera.position, newPosCam, dumping * Time.deltaTime);
    }
}