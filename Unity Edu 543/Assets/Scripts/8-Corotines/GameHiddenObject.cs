﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHiddenObject : MonoBehaviour
{
    private List<string> objectsName;  // список всіх предметів на рівні
    private string objectFind;         // шуканий предмет
    private float timerGame;           // таймер гри
    private float timerTask;           // таймер завдання для пошуку окремого предмета
    private int score=0;               // рахунок
    private Coroutine corGame;
    private Coroutine corTask;

    void Start()
    {
        objectsName = new List<string>();
        HiddenObject[] temp = GetComponentsInChildren<HiddenObject>();
        foreach (var item in temp)
        {
            objectsName.Add(item.name);
        }
        timerGame = objectsName.Count * 10;
        NextObjectToFind();
        corGame = StartCoroutine(TimerGame());
        corTask = StartCoroutine(TimerTask());
    }
   
    void NextObjectToFind()
    {
        timerTask = 0;
        if (objectsName.Count > 0)
        {
            int random = Random.Range(0, objectsName.Count);
            objectFind = objectsName[random];
            objectsName.RemoveAt(random);
            print($"Find: {objectFind}");
        }
        else
        {
            GamaOver(true);
        }
    }

    void GamaOver(bool isWinner)
    {
        string mess = "Game over, you ";
        if (isWinner)
        {
            mess += "winner\nScore: " + score;
        }
        else
        {
            mess += "loser";
        }
        print(mess);
        StopCoroutine(corGame);
        StopCoroutine(corTask);
    }

    IEnumerator TimerGame()
    {
        while (timerGame > 0)
        {
            yield return null;
            timerGame -= Time.deltaTime;
        }
        GamaOver(false);
    }

    IEnumerator TimerTask()
    {
        while (true)
        {
            yield return null;
            timerTask += Time.deltaTime;
        }
    }

    public void AddScore()
    {
        if (timerTask < 3)
        {
            score += 3;
        }
        else if (timerTask < 6)
        {
            score += 2;
        }
        else
        {
            score++;
        }
        print($"Score: {score}\nTime left: {timerGame}");
    }

    public bool IsFind(string name)
    {
        return objectFind == name;
    }

    public void DeleteObject(HiddenObject obj)
    {
        Destroy(obj.gameObject);
        NextObjectToFind();
    }
}
