﻿using System.Collections;
using UnityEngine;

public class CorotineDemo : MonoBehaviour
{
    private Transform _transform;
    private Coroutine cor=null;

    void Start()
    {
        _transform = transform;
        //StartCoroutine(Timer());  
        //StartCoroutine(Anim());
    }

    IEnumerator Timer()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            print("Hello");
            yield return new WaitForSeconds(1);
            print("Unity");     
        }
    }

    IEnumerator Anim()
    {
        while (true)
        {
            _transform.Rotate(20 * Time.deltaTime, 20 * Time.deltaTime, 0);
            yield return null;
        }
    }

    private void OnMouseEnter()
    {
        if (cor == null)
        {
            cor = StartCoroutine(Anim());
        }
    }

    private void OnMouseExit()
    {
        StopCoroutine(cor);
        cor = null;
    }
}
