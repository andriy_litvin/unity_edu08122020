﻿using System.Collections;
using UnityEngine;

public class HiddenObject : MonoBehaviour
{
    private GameHiddenObject game;  // головний скрипт
    private SpriteRenderer sr;      // 

    void Start()
    {
        game = GetComponentInParent<GameHiddenObject>();
        sr = GetComponent<SpriteRenderer>();

    }

    private void OnMouseUpAsButton()
    {
        if (game.IsFind(name))
        {
            StartCoroutine(YesFind()); 
        }
                
    }

    IEnumerator YesFind()
    {
        GetComponent<CircleCollider2D>().enabled = false;
        game.AddScore();
        for (int i = 0; i < 3; i++)
        {
            sr.color = Color.green;
            yield return new WaitForSeconds(0.2f);
            sr.color = Color.white;
            yield return new WaitForSeconds(0.2f);
        }
        game.DeleteObject(this);
    }
}
