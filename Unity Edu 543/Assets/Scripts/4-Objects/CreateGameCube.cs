﻿using UnityEngine;

public class CreateGameCube : MonoBehaviour
{
    public int n = 3;
    public float delta = 0.1f;

    void Start()
    {
        Transform temp;
        for (int x = 0; x < n; x++)
        {
            for (int y = 0; y < n; y++)
            {
                for (int z = 0; z < n; z++)
                {
                    temp = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
                    temp.position = new Vector3(x + delta * x, y + delta * y, z + delta * z);
                    temp.parent = transform;
                    temp.GetComponent<MeshRenderer>().material.color =
                        new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
                }
            }
        }
    }


}
