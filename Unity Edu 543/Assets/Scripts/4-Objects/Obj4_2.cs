﻿using UnityEngine;

public class Obj4_2 : MonoBehaviour
{
    public GameObject cubePref;
    public GameObject spherePref;
    public int n = 5;
    public float delta = 0.1f;

    void Start()
    {
        int num = 0;
        GameObject temp;
        for (int y = 0; y < n; y++)
        {
            for (int x = 0; x < n; x++)
            {
                if (num % 2 == 0)
                {
                    temp = Instantiate(cubePref, new Vector3(x + delta * x, y + delta * y, 0), Quaternion.identity, transform);
                }
                else
                {
                    temp = Instantiate(spherePref, new Vector3(x + delta * x, y + delta * y, 0), Quaternion.identity, transform);
                }
                temp.GetComponent<MeshRenderer>().material.color =
                    new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
                num++;
            }
            if (n % 2 == 0)
            {
                num++;
            }
        }
    }


}
