﻿using UnityEngine;

public class Obj4_4 : MonoBehaviour
{
    public int n = 5;

    void Start()
    {
        for (int x = 0; x < n; x+=n-1)
        {
            for (int z = 0; z < n; z++)
            {
                CreateCube(new Vector3(x, 0, z));
                CreateCube(new Vector3(x, n-1, z));
            }
        }

        for (int x = 1; x < n-1; x++)
        {
            for (int z = 0; z < n; z+=n-1)
            {
                CreateCube(new Vector3(x, 0, z));
                CreateCube(new Vector3(x, n - 1, z));
            }
        }

        for (int y = 1; y < n-1; y++)
        {
            for (int x = 0; x < n; x+=n-1)
            {
                for (int z = 0; z < n; z += n - 1)
                {
                    CreateCube(new Vector3(x, y, z));
                }
            }
        }
    }

    void CreateCube(Vector3 pos)
    {
        GameObject temp = GameObject.CreatePrimitive(PrimitiveType.Cube);
        temp.transform.position = pos;
        temp.transform.parent = transform;
        temp.GetComponent<MeshRenderer>().material.color =
                    new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
    }
}
