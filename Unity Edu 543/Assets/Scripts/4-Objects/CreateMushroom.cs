﻿using UnityEngine;

public class CreateMushroom : MonoBehaviour
{
    public Transform mushroomPref;
    public int countMushroom=5;
    public float posMin = -5;
    public float posMax = 5;
    private Transform _transform;

    void Start()
    {
        _transform = transform;
        for(int i=0; i<countMushroom; i++)
        {
            Instantiate(mushroomPref,
                new Vector3(Random.Range(posMin, posMax), 0, Random.Range(posMin, posMax)),
                Quaternion.identity, _transform);
        }
    }

    
}
