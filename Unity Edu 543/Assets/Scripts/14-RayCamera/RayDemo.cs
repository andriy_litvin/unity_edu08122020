﻿using UnityEngine;

public class RayDemo : MonoBehaviour
{
    public float lengthDrawRay = 1;
    public float deltaMove = 0.05f;
    public float deltaScale = 0.03f;

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Debug.DrawRay(Camera.main.transform.position, ray.direction*lengthDrawRay, Color.red, 2);
            if(Physics.Raycast(ray, out hit))
            {
                print($"{hit.transform.name}   {hit.distance}");
                Debug.DrawLine(Camera.main.transform.position, hit.point, Color.green, 2);
                //hit.transform.position += hit.normal * deltaMove;
                hit.transform.position = Vector3.LerpUnclamped(Camera.main.transform.position, hit.transform.position, 1+deltaMove);
            }
        }
        else if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Debug.DrawRay(Camera.main.transform.position, ray.direction * lengthDrawRay, Color.red, 2);
            if (Physics.Raycast(ray, out hit))
            {
                print($"{hit.transform.name}   {hit.distance}");
                Debug.DrawLine(Camera.main.transform.position, hit.point, Color.green, 2);
                //hit.transform.position += hit.normal * deltaMove;
                hit.transform.position = Vector3.LerpUnclamped(Camera.main.transform.position, hit.transform.position, 1 - deltaMove);
            }
        }
        else if(Input.GetMouseButton(2))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;            
            if (Physics.Raycast(ray, out hit))
            {
                hit.transform.localScale *= Input.GetAxis("Mouse Y") * deltaScale + 1;
            }
        }

    }
}
