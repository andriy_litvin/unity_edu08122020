﻿using System.Collections;
using UnityEngine;

public class CameraDemo2 : MonoBehaviour
{
    public Camera cam1;
    public Camera cam2;
    Vector4 rectFull;
    Vector4 rectMin;
    Vector4 rectTemp;

    void Start()
    {
        rectFull = new Vector4(0, 0, 1, 1);
        rectMin = new Vector4(0.02f, 0.03f, 0.3f, 0.3f);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            StartCoroutine(AnimCamera(cam1, cam2));
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            StartCoroutine(AnimCamera(cam2, cam1));
        }
    }

    IEnumerator AnimCamera(Camera camMax, Camera camMin)
    {
        camMax.depth = 1;
        camMin.depth = 0;

        while(true)
        {
            yield return null;
            rectTemp = new Vector4(camMax.rect.x, camMax.rect.y, camMax.rect.width, camMax.rect.height);
            rectTemp = Vector4.MoveTowards(rectTemp, rectFull, 0.05f);
            camMax.rect = new Rect(rectTemp.x, rectTemp.y, rectTemp.z, rectTemp.w);
            if(Vector4.Distance(rectTemp, rectFull)<0.001f)
            {
                break;
            }
        }
        camMin.rect = new Rect(rectMin.x, rectMin.y, rectMin.z, rectMin.w);
        camMax.depth = 0;
        camMin.depth = 1;
    }
}
