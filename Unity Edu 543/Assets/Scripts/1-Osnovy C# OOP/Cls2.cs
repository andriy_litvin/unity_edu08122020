﻿using UnityEngine;
using System;

public class Animal
{
    public string name;
    public string color;
    protected float height;
    private float weight;

    public Animal(string name, string color, float height, float weight)
    {
        this.name = name;
        this.color = color;
        this.height = height;
        this.weight = weight;
    }

    public void Run()
    {
        Debug.Log(name+": run");
    }
}

public class Dog : Animal
{
    public string poroda;

    public Dog(string name, string color, float height, float weight, string poroda): base(name, color, height, weight)
    {
        this.poroda = poroda;
    }

    public void Voise()
    {
        Debug.Log(name + ":woof-woof");
        
    }

    public void Atack()
    {
        Debug.Log(name + ":rrr...");
    }
}

public class Cat:Animal
{
    public int counterCatchMouse;

    public Cat(string name, string color, float height, float weight, int counterCatchMouse) : base(name, color, height, weight)
    {
        this.counterCatchMouse = counterCatchMouse;
    }

    public void Voise()
    {
        Debug.Log(name + ":mur-myav");
    }

    public void FindMouse()
    {
        Debug.Log(name + ":mur-find mouse");
    }
}



public class MyData
{
    public int day;
    public int month;
    public int year;

    public MyData()
    {
        DateTime date = DateTime.Today;
        day = date.Day;
        month = date.Month;
        year = date.Year;
    }

    public MyData(int day, int month, int year)
    {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public bool NoMonthEqalNoDay()
    {
        return day == month;
    }

    public void AddMonth()
    {
        /*month++;
        if(month>12)
        {
            month = 1;
            year++;
        }
        */

        DateTime date = new DateTime(year, month, day);
        date = date.AddMonths(1);
        day = date.Day;
        month = date.Month;
        year = date.Year;        
    }

    public string toString()
    {
        return $"{day}.{month}.{year}";
    }
}

public class Liki: MyData
{
    public string nameLiki;
    public string nameCompany;

    public Liki()
    {
        nameLiki = "no name";
        nameCompany = "no name";
    }

    public Liki(string nameLiki, string nameCompany)
    {
        this.nameLiki = nameLiki;
        this.nameCompany = nameCompany;
    }

    public Liki(string nameLiki, string nameCompany, int day, int month, int year): base(day, month, year)
    {
        this.nameLiki = nameLiki;
        this.nameCompany = nameCompany;
    }

    public int DaysFromDateCreate()
    {
        DateTime date = new DateTime(year, month, day);
        var sdata = DateTime.Now.Subtract(date);
        return (int) sdata.TotalDays;
    }

    public string toString()
    {
        return $"{nameLiki}  {nameCompany}  " + base.toString();
    }
}
