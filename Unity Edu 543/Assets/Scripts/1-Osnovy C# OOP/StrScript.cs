﻿using UnityEngine;

public class StrScript : MonoBehaviour
{
    void Start()
    {
        /*string s = "Hello";
        char c = s[1];

        s += " Unity"+'!';
        print(s);
        print(s.Length);

        //   \t  \n   \\   \"
        s = "Hello\t\"Unity\"";
        print(s);
        s = "\\Hello\nUnity!";
        print(s);*/

        /*string s1 = null, s2 = "", s3 = "Hello";
        string.IsNullOrEmpty(s1); // true
        string.IsNullOrEmpty(s2); // true
        string.IsNullOrEmpty(s3); // false

        string s4 = "\t";
        string.IsNullOrWhiteSpace(s1); // true
        string.IsNullOrWhiteSpace(s2); // true
        string.IsNullOrWhiteSpace(s3); // false
        string.IsNullOrWhiteSpace(s4); // true
        */

        /*
        print(string.Compare("abc", "abc")); //0
        print(string.Compare("abc", "a")); //1
        print(string.Compare("a", "b")); // -1
        print(string.Compare("Pasha", "Mash")); // 1

        print(string.Compare("abc", "Abc")); //-1
        print(string.Compare("abc", "Abc",true)); //0
        */

        /*
        string s = "Hello";
        s = s.ToUpper();
        print(s);
        s = s.ToLower();
        print(s);
        */
        /*
        string s = "Hello Unity";
        s.Contains("Hello"); // true
        if(s.Contains("Unity"))
        {
            print("Unity uraaa");
        }
        else
        {
            print("nema");
        }
        */
        /*
        string s = "Hello Unity";
        print(s.IndexOf("ll")); //2
        print(s.IndexOf("a"));  //-1
        */
        /*
        string s = "Hello Unity";
        s = s.Insert(5, ",");
        print(s);

        print(s.Remove(7));
        print(s.Remove(5, 1));

        print(s.Substring(7));
        print(s.Substring(0,5));
        */
        /*
        string s = "Hello Unity, Hello World";
        s = s.Replace("Hello", "Hi");
        print(s);

        char[] arr = s.ToCharArray();
        foreach (var item in arr)
        {
            print(item);
        }
        */
        /*
        string s = "Іван,Коля,Саша,Маша,Петя";
        string[] arr = s.Split(',');
        foreach (var item in arr)
        {
            print(item);
        }
        */


        // 1
        /*string s = "програма";
        string s1 = s.Substring(1, 2) + s.Substring(6, 1);
        string s2 = s.Substring(4);
        print(s1);
        print(s2);
        */
        /*
        string s = "мама мила раму, раму мила мама";
        string hol = "аоуиіе";
        int count = 0;
        for (int i = 0; i < hol.Length; i++)
        {
            for (int j = 0; j < s.Length; j++)
            {
                if (hol[i] == s[j])
                {
                    count++;
                }
            }
        }
        print(count);
        */

        // 11
        string s = "мама мила раму, раму мила мама";
        //string s = "аа ила рау, рау ила аа"; 1
        //string s = " ил ру, ру ил "; 2
        //string s = "илру,руил"; 3
        //string s = "лру,рул"; 4
        //string s = "ру,ру"; 5
        //string s = "у,у"; 6

        int count =0;
        
        while(s.Length>0)
        {
            s = s.Replace(s[0].ToString(), "");
            count++;
        }
        print(count);



    }

}
