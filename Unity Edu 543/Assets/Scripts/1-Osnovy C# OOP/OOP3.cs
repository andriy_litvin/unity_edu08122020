﻿using UnityEngine;
using System.Collections.Generic;

public class OOP3 : MonoBehaviour
{
    void Start()
    {
        /*Animal2[] animals = new Animal2[4];

        animals[0] = new Cat2("murko", "grey", 5);
        animals[1] = new Dog2("sirko", "dark", 10);
        animals[2] = new Animal2("animal", "red");
        animals[3] = new Cat2("vasja", "white", 8);

        foreach (var item in animals)
        {
            item.Info();
            item.Voise();
        }*/

        /*Vint vint = new Vint("intel", 3100, 8000, 1000);
        vint.Info();
        print(vint.Quality());    */

        /*List<Transport2> trans = new List<Transport2>();
        trans.Add(new Car2("Opel", "12344AA", 200, 4));
        trans.Add(new Plane2("Boing", "12548D", 850, 4));
        trans.Add(new Car2("Tesla", "5488AB", 240, 5));

        foreach (var item in trans)
        {
            item.Info();
        }*/

        string[] names = {"Олександр", "Андрій", "Анастасія", "Ірина",
            "Наталя", "Павло", "Роман", "Світлана","Сергій", "Тетяна"};

        Human2[] humans = new Human2[5];
        for (int i = 0; i < humans.Length; i++)
        {
            int randHuman = Random.Range(0, 3);
            switch (randHuman)
            {
                case 0:
                    humans[i] = new Formalist(names[Random.Range(0, names.Length)], Random.Range(20, 41));
                    break;
                case 1:
                    humans[i] = new Neformalist(names[Random.Range(0, names.Length)], Random.Range(20, 41));
                    break;
                case 2:
                    humans[i] = new Realist(names[Random.Range(0, names.Length)], Random.Range(20, 41));
                    break;
                default:
                    break;
            }
        }

        foreach (var item in humans)
        {
            print(item.About());
        }

        for (int i = 0; i < humans.Length; i++)
        {
            for (int j = i+1; j < humans.Length; j++)
            {
                print(humans[i].name + ": " + humans[i].Hello(humans[j]));
                print(humans[j].name + ": " + humans[j].Hello(humans[i]));
            }
        }



    }


}
