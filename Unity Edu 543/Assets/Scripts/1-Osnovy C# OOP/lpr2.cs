﻿using UnityEngine;

public class lpr2 : MonoBehaviour
{
    void Start()
    {
        // 2 a
        float a = Random.Range(0, 20);
        float b = Random.Range(0, 20);
        float c = Random.Range(0, 20);
        float x = Random.Range(0, 20);

        float rezul = 1 / Mathf.Sqrt(a * Mathf.Pow(x, 2) + b * x + c);
        print("2a = " + rezul);

        // 6
        // a^6 = a*a*a*a*a*a   (за 3 *)
        // a2 = a*a;
        // a4 = a2 * a2;
        // a6 = a4 * a2;

        // -8-
        // 123  ->  312
        int number = 123;
        int n3 = number % 10;
        n3 *= 100; // 300
        number /= 10; // 12
        number += n3;
        print(number);

        // -9-
        // 1234  -> 1+2+3+4=10
    }

}
