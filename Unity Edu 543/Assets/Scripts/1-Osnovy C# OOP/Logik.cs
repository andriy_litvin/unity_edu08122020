﻿using UnityEngine;

public class Logik : MonoBehaviour
{
    void Start()
    {
             

        // &&   -  і
        // ||   -  або
        //  < > <= >= == !=

        /*bool b = false; // true
        bool a = true;

        bool c = a && b; // false
        c = a && true; // true

        c = a || b; // true
        c = !a || b; // false

        c = 3 > 6 && 5 >= 3; // false  
        c = 4 != 4 || (3 < 8 && true); // true
        */
        // &
        // 1100
        // 1010

        // 1000

        // |
        // 1100
        // 1010

        // 1110

        // ^
        // 1100
        // 1010

        // 0110

        // ~
        // 1100

        // 0011

        // <<
        // 1010 1000 << 2

        // 1010 0000

        // >>
        // 1010 1000 >> 2

        // 1110 1010


        // ~25

        /*int num = Random.Range(-4,5);
        print(num);
        if (num > 0)
        {
            print("+++");
        }
        else if(num < 0)
        {            
            print("---");
        }
        else
        {
            print("-0-");
        }*/
        /*
        int days = Random.Range(0, 10); // 0 - понеділок ... 6 - неділя 7,8,9
        print(days);
        switch (days)
        {
            case 0:
                print("понеділок");
                break;
            case 1:
                print("вівторок");
                break;
            case 2:
                print("середа");
                break;
            case 3:
                print("четвер");
                break;
            case 4:
                print("п'ятниця");
                break;
            case 5:
                print("субота");
                break;
            case 6:
                print("неділя");
                break;
            default:
                print("такого дня немає");
                break;
        }


        switch (days)
        {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                print("будні");
                break;
            case 5:
            case 6:
                print("вихідні");
                break;
            default:
                print("такого дня немає");
                break;
        }
        */

        // якщо випадає парне число rand, то додаємо а та b, інакше віднімаємо 
        /*int a = 3, b = 4;
        int rand = Random.Range(0, 10);
        print(rand);
        int rezul = rand % 2 == 0 ? a + b : a - b;
        print(rezul);*/
        /*
        int a = 3, b = 4;
        int rand = Random.Range(-10, 10);
        print(rand);
        int rezul = 0;
        if (rand % 2 == 0)
            rezul = a + b;
        else
            rezul = a - b;
        print(rezul);


        char ch = rand > 0 ? '+' : (rand < 0 ? '-' : '*');
        print(ch);
        */

        // lpr3-3
        int a = 50, b = 80; // конверт
        int c = Random.Range(40,100), d = Random.Range(40, 100);  // листивка

        if((c+2<=a && d+2 <=b)||(c+2 <= b && d+2 <=a))
        {
            Debug.LogFormat("листівка ({0}x{1}) вміщається в конверт ({2}x{3})", c,d,a,b);
        }
        else
        {
            Debug.LogFormat("листівка ({0}x{1}) не вміщається в конверт ({2}x{3}), потрібен більший конверт", c, d, a, b);
            
        }

        // lpl4-4
        /*int n = 4;
        int n2 = 1 << n;
        Debug.LogFormat("2^{0} = {1}", n, n2);
        print("2^" + n + " = " + n2);
        */

        // lpr4-6
        /*int n = 15;
        int i = 5;
        int rezul = 1 << i | n;
        print(rezul);
        */

        // lpr4-5
        int n = 47;
        string s = "";

        s =  (n & (1 << 0)) != 0 ? "1" : "0";
        s = ((n & (1 << 1)) != 0 ? "1" : "0") + s;
        s = ((n & (1 << 2)) != 0 ? "1" : "0") + s;
        s = ((n & (1 << 3)) != 0 ? "1" : "0") + s;
                            
        s = ((n & (1 << 4)) != 0 ? "1" : "0") + s;
        s = ((n & (1 << 5)) != 0 ? "1" : "0") + s;
        s = ((n & (1 << 6)) != 0 ? "1" : "0") + s;
        s = ((n & (1 << 7)) != 0 ? "1" : "0") + s;

        print(s);
    }


}
