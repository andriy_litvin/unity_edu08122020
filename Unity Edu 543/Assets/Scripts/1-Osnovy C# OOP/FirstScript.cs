﻿using UnityEngine;

public class FirstScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        print("Hello\nUnity\t2020");
        Debug.Log("log");
        Debug.LogWarning("warning");
        Debug.LogError("error");
        Debug.LogFormat("log {0}    {1}", 2, 6);

    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("log");
    }
}
