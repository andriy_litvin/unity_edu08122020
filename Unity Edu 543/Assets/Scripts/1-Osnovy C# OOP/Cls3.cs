﻿using UnityEngine;

public class Animal2
{
    public string name;
    public string color;

    public Animal2(string name, string color)
    {
        this.name = name;
        this.color = color;
    }

    public virtual void Info()
    {
        Debug.Log($"Name: {name}, Color: {color}");
    }

    public virtual void Voise()
    {
        Debug.Log("hhh...");
    }
}

public class Dog2:Animal2
{
    public int countBone;

    public Dog2(string name, string color, int countBone):base(name, color)
    {
        this.countBone = countBone;
    }

    public override void Info()
    {
        Debug.Log($"Name: {name}, Count bone: {countBone}, Color: {color}");
    }

    public override void Voise()
    {
        Debug.Log("gav-gav");
        base.Voise();
    }
}

public class Cat2: Animal2
{
    public int countMouse;

    public Cat2(string name, string color, int countMouse): base(name, color)
    {
        this.countMouse = countMouse;
    }

    public override void Info()
    {
        Debug.Log($"Name: {name}, Count mouse: {countMouse}, Color: {color}");
    }

    public override void Voise()
    {
        Debug.Log("miv-miv");
    }
}

public class Comp
{
    public string nameProc;
    public float chastotaProc;
    public int ram;

    public Comp(string nameProc, float chastotaProc, int ram)
    {
        this.nameProc = nameProc;
        this.chastotaProc = chastotaProc;
        this.ram = ram;
    }

    public virtual float Quality()
    {
        return (0.1f * chastotaProc) + ram;
    }

    public virtual void Info()
    {
        Debug.Log($"Назва процесора: {nameProc}, Частота процесора: {chastotaProc}," +
            $" Об'єм ОЗУ: {ram}");
    }
}

public class Vint:Comp
{
    public int memory;

    public Vint(string nameProc, float chastotaProc, int ram, int memory):
        base(nameProc, chastotaProc, ram)
    {
        this.memory = memory;
    }

    public override float Quality()
    {
        return base.Quality()+0.5f*memory;
    }

    public override void Info()
    {
        Debug.Log($"Назва процесора: {nameProc}, Частота процесора: {chastotaProc}," +
            $" Об'єм ОЗУ: {ram}, Об'єм памяті: {memory}");
    }
}


public abstract class Transport2
{
    public string model;
    public string nomer;
    public int maxSpeed;

    public abstract void Info();
    
}

public class Car2: Transport2
{
    public int countDoor;

    public Car2(string model, string nomer, int maxSpeed, int countDoor)
    {
        this.model = model;
        this.nomer = nomer;
        this.maxSpeed = maxSpeed;
        this.countDoor = countDoor;
    }

    public override void Info()
    {
        Debug.Log($"Модель: {model}, Номер: {nomer}, Максимальна швидкість: {maxSpeed}, Кількість дверей: {countDoor}");
    }
}

public class Plane2:Transport2
{
    public int countEngine;

    public Plane2(string model, string nomer, int maxSpeed, int countEngine)
    {
        this.model = model;
        this.nomer = nomer;
        this.maxSpeed = maxSpeed;
        this.countEngine = countEngine;
    }

    public override void Info()
    {
        Debug.Log($"Модель: {model}, Номер: {nomer}, Максимальна швидкість: {maxSpeed}, Кількість двигунів: {countEngine}");
    }
}



public abstract class Human2
{
    public string name;
    public int age;

    public abstract string Hello(Human2 human);
    public abstract string About();
}

public class Formalist:Human2
{
    public Formalist(string name, int age)
    {
        this.name = name;
        this.age = age;
    }

    public override string Hello(Human2 human)
    {
        return "Доброго дня, " + human.name;
    }

    public override string About()
    {
        return $"Мене звати {name}, мій вік {age} років, я формаліст";
    }
}

public class Neformalist : Human2
{
    public Neformalist(string name, int age)
    {
        this.name = name;
        this.age = age;
    }

    public override string Hello(Human2 human)
    {
        return "Привіт, " + human.name;
    }

    public override string About()
    {
        return $"Мене звати {name}, мій вік {age} років, я неформаліст";
    }
}

public class Realist : Human2
{
    public Realist(string name, int age)
    {
        this.name = name;
        this.age = age;
    }

    public override string Hello(Human2 human)
    {
        if(human.age<=age || human.age-age<=5)
            return "Привіт, " + human.name;
        else
            return "Доброго дня, " + human.name;

    }

    public override string About()
    {
        return $"Мене звати {name}, мій вік {age} років, я реаліст";
    }
}