﻿
using UnityEngine;

public class OOP1 : MonoBehaviour
{
    void Start()
    {
        /*Human human = new Human();
        human.PrintInfo();

        Human human1 = new Human("Petya", Sex.man, 2019, 10, 70, 0);
        print(human1.Info());
        */

        /*Stepin st = new Stepin(2, 4);
        st.Display();
        print(st.Power());
        Stepin st2 = new Stepin(3, 3);*/


        ComplexNum num1 = new ComplexNum(1, 2);
        ComplexNum num2 = new ComplexNum(3, 4);

        /*Complex com = new Complex();
        ComplexNum rezul = com.Add(num1, num2);
        rezul.Display();*/

        ComplexNum rezul = Complex.Add(num1, num2);
        rezul.Display();

        
    }


}
