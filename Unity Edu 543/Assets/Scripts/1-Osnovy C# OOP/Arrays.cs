﻿using UnityEngine;

public class Arrays : MonoBehaviour
{
    //public int[] arr = new int[3];

    void Start()
    {
        /*int[] m = new int[8];
        int[] m2 = { 1, 4, 7, 9 };

        for (int i = 0; i < 8; i++)
        {
            m[i] = Random.Range(1, 10);
        }

        string s = "";
        for (int i = 0; i < m.Length; i++)
        {
            s += m[i] + " ";
        }
        print(s);*/

        // 3
        /*int[] m3 = new int[20];
        for (int i = 0; i < m3.Length; i++)
        {
            m3[i] = 20 - i;
        }
        string s = "";
        foreach (var item in m3)
        {
            s += item + " ";
        }
        print(s);*/

        // 4
        /*int n = Random.Range(5, 21); // 5..20
        int[] m3 = new int[n];
        for (int i = 0; i < m3.Length; i++)
        {
            m3[i] = Random.Range(-100,100);
        }

        int suma=0;
        foreach (var item in m3)
        {
            suma += item;
        }

        string s = "";
        foreach (var item in m3)
        {
            s += item + " ";
        }
        print(s);

        print(suma);
        if(suma>=0)
        {
            print("Сума елементів масиву є невідємним числом");
        }
        else
        {
            print("Сума елементів масиву є відємним числом");
        }*/

        // 9
        int[] m3 = new int[10];
        for (int i = 0; i < m3.Length; i++)
        {
            m3[i] = Random.Range(1, 50);
        }
        string s = "";
        foreach (var item in m3)
        {
            s += item + " ";
        }
        print("Невідсортований: "+ s);
        for (int i = 0; i < m3.Length-1; i++)
        {
            int min = m3[i];
            int minIndex = i;
            for (int j = i+1; j < m3.Length; j++)
            {
                if(m3[j]<min)
                {
                    min = m3[j];
                    minIndex = j;
                }
            }
            int temp = m3[i];
            m3[i] = m3[minIndex];
            m3[minIndex] = temp;
        }
        s = "";
        foreach (var item in m3)
        {
            s += item + " ";
        }
        print("Відсортований: " + s);
    }
}
