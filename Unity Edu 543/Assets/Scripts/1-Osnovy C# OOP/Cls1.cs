﻿using UnityEngine;

public class Human
{
    // поля
    public string name; // ім'я
    public Sex sex;     // стать
    private int _yearOfBirth; // рік народження
    private int _height;  // зріст
    private int _weight;  // вага
    private int _money;  // гроші

    // властивості
    public string ColorOfEyes { get;  }

    public int Height
    {
        get
        {
            return _height;
        }

        set
        {
            if (value > 0)
                _height = value;
            else
                Debug.LogWarning("Не коректний зріст"); 
        }
    }

    public int Age
    {
        get
        {
            return System.DateTime.Now.Year - _yearOfBirth;
        }
    }

    // конструктор
    public Human()
    {
        name = "no name";
        sex = Sex.none;
        _yearOfBirth = System.DateTime.Now.Year;
        _weight = 1;
        _height = 1;
        _money = 0;
    }

    public Human(string name, Sex sex, int yearOfBirth, int weight, int height, int money)
    {
        this.name = name;
        this.sex = sex;
        _yearOfBirth = yearOfBirth;
        _weight = weight;
        _height = height;
        _money = money;
    }

    // деструктор
    ~Human()
    {
        Debug.Log("pa-pa");
    }


    // методи
    public void PrintInfo()
    {
        Debug.LogFormat("{0}  {1}  {2}  {3}  {4}  {5}", name, sex, _yearOfBirth, Age, _height, _weight);
    }

    public string Info()
    {
        return $"{name}  {sex}  {_yearOfBirth}  {Age}  {_height}  {_weight}";
    }

}


public enum Sex
{
    none,
    man,
    woman
}

public struct Demo
{

}


public class Stepin
{
    public float first;
    public int second;

    public Stepin()
    {
        first = 1;
        second = 1;
    }

    public Stepin(float first, int second)
    {
        this.first = first;
        this.second = second;
    }

    public void Display()
    {
        Debug.LogFormat("{0} {1}", first, second);
    }

    public float Power()
    {
        return Mathf.Pow(first, second);
    }

}


public class ComplexNum
{
    public float a;
    public float b;

    public ComplexNum()
    {
        a = 0;
        b = 0;
    }

    public ComplexNum(float a, float b)
    {
        this.a = a;
        this.b = b;
    }

    public void Display()
    {
        Debug.LogFormat("{0}, {1}", a, b);
    }

    public string toString()
    {
        return $"{a}, {b}";  //a.ToString() + ", " + b.ToString();
    }
}


public static class Complex
{

    public static ComplexNum Add(ComplexNum num1, ComplexNum num2)
    {
        return new ComplexNum(num1.a + num2.a, num1.b + num2.b);
    }

    public static ComplexNum Sub(ComplexNum num1, ComplexNum num2)
    {
        return new ComplexNum(num1.a - num2.a, num1.b - num2.b);
    }
}