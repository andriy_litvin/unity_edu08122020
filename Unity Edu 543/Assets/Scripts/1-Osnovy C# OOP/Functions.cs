﻿using UnityEngine;

public class Functions : MonoBehaviour
{
    void Start()
    {
        //print(Max(35, 44));
        /*int[] arr = {1,2,6,3,7,2,8,2};
        string[] name = { "Іван", "Саша", "Маша", "Коля", "Даша" };

        PrintArray(arr);
        PrintArray(name);*/

        /*
        Info("Ivan", 20);

        Info(age: 50, name: "Kolya");

        Info();
        Info("Sasha");
        Info(age: 20);
        */
        /*
        int a;
        Add(out a);
        print("start "+a);
        */
        /*
        print(Suma(2, 4, 7));
        print(Suma(1, 2, 3, 4, 5, 6, 7, 8));
        print(Suma(1, 2, 7, 8));
        */

        // 3
        //print(CountCharInStr("мама мила раму", 'а'));

        // 2
        //print(SumaNums(123));

        print(Factorial(5));

        print(Rec(1, 3, 5));


    }

    int Rec(int n1, int delta, int n)
    {
        if (n == 1)
            return n1;
        else
            return Rec(n1, delta, n - 1) + delta;
    }


    int Factorial(int n)
    {
        if (n == 1)
            return 1;
        else
            return Factorial(n - 1) * n;
    }

    // 2
    int SumaNums(int num)
    {
        string str = num.ToString();
        int suma = 0;

        foreach (var item in str)
        {
            suma +=  int.Parse(item.ToString());
        }
        return suma;
    }



    // 3
    int CountCharInStr(string str, char charFind)
    {
        int count = 0;
        foreach (var item in str)
        {
            if(item==charFind)
            {
                count++;
            }
        }
        return count;
    }


    int Suma(params int[] nums)
    {
        int suma = 0;
        foreach (var item in nums)
        {
            suma += item;
        }
        return suma;
    }


    /*
    // ref   out
    void Add(out int a)
    {
        a = 0;
        a++;
        print("fun "+a);
    }
    */

    void Info(string name = "You", int age = 1)
    {
        Debug.LogFormat("Ім'я: {0} Вік: {1}", name, age);
    }

    void PrintArray(int[] array)
    {
        string s = "";
        foreach (var item in array)
        {
            s += item + " ";
        }
        print(s);
    }

    void PrintArray(string[] array)
    {
        string s = "";
        foreach (var item in array)
        {
            s += item + " ";
        }
        print(s);
    }


    int Max(int a, int b)
    {
        if (a > b)
            return a;
        else
            return b;
    }



}
