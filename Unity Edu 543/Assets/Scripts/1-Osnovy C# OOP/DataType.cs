﻿using UnityEngine;

public class DataType : MonoBehaviour
{
    const float PI = 3.1415f;

    // глобальні змінні (поля класу)
    public int intVal;
    public float floatVal;
    public bool boolVal;
    public string strVal;
    [SerializeField]
    int val1 = 10;


    void Start()
    {
        /*
        // локальні змінні
        int a; // оголошення змінної типу int
        a = 0; // ініціалізація
        float b = 0.6f;
        bool c = false;
        string s = "hello";
        char ch = ' ';

        var varTest = "text";

        a = (int) b + val1;
        print(a);
        a = (int)Mathf.Round(b + val1);
        print(a);

        print(PI);*/


        // + - * / %
        /*int a=8, b=3;
        float c = (float)a / b;
        print(c);

        int x = 8 % 3;
        // 3   3   2   =  8

        int r = Random.Range(0, 20); //0,1,2....19
        print(r);
        print(r % 2);*/


        // a = a + 1
        //  a++
        //  ++a

        //  a = a - 1
        //  a--
        //  --a

        int a = 5;
        int b = a++ + 3;        
        // b = 8
        // a = 6

        a = 5;
        b = ++a + 3;
        // b = 9
        // a = 6

        //  +=   -=  *=  /=  %=
        // a = a + 4  <->  a+=4

        a = 3;
        b = a++;  // b = 3   a = 4
        a += 2;   // a  =  6   
        b = ++a - 3;  // b = 4   a = 7

    }

    void Update()
    {
        

    }
}
