﻿using UnityEngine;

public class CameraMoveDemo : MonoBehaviour
{
    public float speed = 1;
    private Transform _transform; 

    void Start()
    {
        _transform = transform;  
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            _transform.position += Vector3.right * Input.GetAxis("Mouse X") * speed * Time.deltaTime;
        }
        if (Input.GetMouseButton(1))
        {
            _transform.position += Vector3.up * Input.GetAxis("Mouse Y") * speed * Time.deltaTime;
        }
        _transform.position += Vector3.forward * Input.GetAxis("Mouse ScrollWheel") * speed;
        
    }
}
