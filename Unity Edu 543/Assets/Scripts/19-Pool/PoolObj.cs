﻿using UnityEngine;

public class PoolObj : MonoBehaviour
{
    public int idPool;

    public void DestroyObj()
    {
        PoolSys.sys.ReturnInPool(idPool, this);
    }
}
