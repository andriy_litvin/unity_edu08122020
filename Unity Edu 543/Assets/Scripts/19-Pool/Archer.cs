﻿using UnityEngine;

public class Archer : MonoBehaviour
{


    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Rigidbody2D temp= PoolSys.sys.TakeFromPool(0, transform.position).GetComponent<Rigidbody2D>();
            temp.AddForce(new Vector2(100, 0));
        }
        else if(Input.GetMouseButtonDown(1))
        {
            Rigidbody2D temp = PoolSys.sys.TakeFromPool(1, transform.position).GetComponent<Rigidbody2D>();
            temp.AddForce(new Vector2(150, 0));

        }
    }
}
