﻿using System.Collections.Generic;
using UnityEngine;

public class PoolSys : MonoBehaviour
{
    public static PoolSys sys; // статична змінна для доступу до пула
    public PoolObj[] poolObjs; // масив об'єктів/префабів, що формуватимуть пули
    private List<PoolObj>[] pool; // масив списків - об'єкти, що знаходяться в пулах 
    private int[] countActiv; // кількість активних об'єктів в кожному пулі по окремості
    private Transform[] poolParent; // батьківський об'єкт для групування/упорядковування об'єктів пулу в ієрархії

    private void Awake()
    {
        sys = this;
        pool = new List<PoolObj>[poolObjs.Length];
        poolParent = new Transform[poolObjs.Length];
        for(int i=0; i<pool.Length; i++)
        {
            pool[i] = new List<PoolObj>();
            poolParent[i] = new GameObject().transform;
            poolParent[i].parent = transform;
            poolParent[i].name = poolObjs[i].name;
        }
        countActiv = new int[pool.Length];
    }

    // додавання/створення об'єктів в пул
    private GameObject AddToPool(int idPool, Vector3 pos)
    {
        PoolObj temp = Instantiate(poolObjs[idPool], pos, Quaternion.identity, poolParent[idPool]);
        pool[idPool].Add(temp);
        temp.idPool = idPool;
        return temp.gameObject;
    }

    // отримання об'єкта з пула
    public GameObject TakeFromPool(int idPool, Vector3 pos)
    {
        GameObject temp = null;
        if(countActiv[idPool]<pool[idPool].Count)
        {
            for (int i = 0; i < pool[idPool].Count; i++)
            {
                if(!pool[idPool][i].gameObject.activeSelf)
                {
                    temp = pool[idPool][i].gameObject;
                    temp.transform.position = pos;
                    break;
                }
            }
        }
        if(!temp)
        {
            temp = AddToPool(idPool, pos);
        }
        temp.SetActive(true);
        countActiv[idPool]++;
        return temp;
    }

    // повернення об'єкта в пул
    public void ReturnInPool(int idPool, PoolObj obj)
    {
        countActiv[idPool]--;
        obj.gameObject.SetActive(false);
    }
}
