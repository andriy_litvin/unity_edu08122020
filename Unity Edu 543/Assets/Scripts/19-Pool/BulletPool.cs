﻿using UnityEngine;

public class BulletPool : MonoBehaviour
{
    private PoolObj poolObj;

    void Start()
    {
        poolObj = GetComponent<PoolObj>();
    }

    private void OnBecameInvisible()
    {
        poolObj.DestroyObj();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        poolObj.DestroyObj();

    }

}
